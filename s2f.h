#ifndef META_S2F_H
#define META_S2F_H

#include "util.h"
#include "decimal.h"
#include "string.h"

#define FLOAT(f) ::tml::s_to_f<STR( #f )>

namespace tml {

// # --------------------------------------------------------------------------------

template<typename, char ...>
struct String2FloatImpl3;

template<typename F, char C, char ... S>
struct String2FloatImpl3<F, C, S...>
{
	static_assert(C >= '0' && C <= '9', "C must be a valid digit!");

	typedef decimal::Add<
		decimal::Div<
			decimal::to_f<C - '0',0>,
			F>,
		typename String2FloatImpl3<
			decimal::Mul<
				decimal::to_f<10,0>,
				F>,
			S...>::Type
	> Type;
};

template<typename F>
struct String2FloatImpl3<F>
{
	typedef decimal::to_f<0,0> Type;
};

template<typename, char ...>
struct String2FloatImpl2;

template<typename R, char C, char ... S>
struct String2FloatImpl2<R, C, S...>
	: String2FloatImpl2<
		decimal::Add<
			decimal::Mul<
				R,
				decimal::to_f<10,0>>,
			decimal::to_f<(C - '0'),0>>,
		S...>
{
	static_assert(C >= '0' && C <= '9', "C must be a valid digit!");
};

template<typename R, char ... S>
struct String2FloatImpl2<R, '.', S...>
{
	typedef decimal::Add<
		R,
		typename String2FloatImpl3<
			decimal::to_f<10,0>,
			S...>::Type
	> Type;
};

template<typename R>
struct String2FloatImpl2<R>
{
	typedef R Type;
};

template<char ... S>
struct String2FloatImpl1 : String2FloatImpl2<decimal::to_f<0,0>, S...>
{
};

template<char ... S>
struct String2FloatImpl1<'-', S...>
{
	typedef decimal::Mul<
		decimal::to_f<-1,0>,
		typename String2FloatImpl2<decimal::to_f<0,0>, S...>::Type
	> Type;
};

template<typename>
struct String2Float;

template<char ... C>
struct String2Float<String<Char<C>...>> : String2FloatImpl1<C...>
{
};

template<typename S>
using s_to_f = typename String2Float<S>::Type;

// # --------------------------------------------------------------------------------

template<int32_t I, char ... S>
struct F2S_Int : F2S_Int<(I / 10), ((I % 10) + '0'), S...>
{
};

template<char ... S>
struct F2S_Int<0, S...>
{
	typedef String<Char<S>...> Type;
};

template<>
struct F2S_Int<0>
{
	typedef String<Char<'0'>> Type;
};

template<typename F, int N>
struct F2S_Frac
{
	typedef decimal::Mul<F, decimal::to_f<10,0>> shft_t;
	typedef decimal::Sub<shft_t, decimal::to_f<decimal::to_i<shft_t>::Value,0>> nxt_t;

	typedef typename Join<
		String<Char<decimal::to_i<shft_t>::Value + '0'>>,
		typename If<
			decimal::IsZero<nxt_t>,
			Type2Type<String<>>,
			F2S_Frac<nxt_t, (N - 1)>
		>::Type
	>::Type Type;
};

template<typename F>
struct F2S_Frac<F, 0>
{
	typedef String<> Type;
};

template<bool S, int32_t I, typename D, int N>
struct F2S_Impl
	: Join<
		typename F2S_Int<I>::Type,
		typename If<
			decimal::IsZero<D>,
			Type2Type<String<>>,
			Join<
				String<Char<'.'>>,
				typename F2S_Frac<D, N>::Type
			>
		>::Type
	>
{
};

template<int32_t I, typename D, int N>
struct F2S_Impl<true, I, D, N>
	: Join<
		String<Char<'-'>>,
		typename F2S_Impl<
			false,
			-I,
			decimal::Mul<
				D,
				decimal::to_f<-1,0>
			>,
			N
		>::Type
	>
{
};

static constexpr int DISPLAY_DIGITS = 15;

template<typename F, int N = DISPLAY_DIGITS>
struct Float2String
	: F2S_Impl<
		decimal::IsNegative<F>::Value,
		decimal::to_i<F>::Value,
		decimal::Sub<F, decimal::to_f<decimal::to_i<F>::Value,0>>,
		N
	>
{
};

template<typename F, int N = DISPLAY_DIGITS>
using f_to_s = typename Float2String<F, N>::Type;

// # --------------------------------------------------------------------------------

}

#endif

