#include "tml.h"

namespace test
{
	using namespace tml;

	template<typename ...> struct VA { };

	typedef make_list<int, float, char, double> mylist_t;

	STATIC_ASSERT((Equals<int,    nth<mylist_t, Int<0>>>::Value));
	STATIC_ASSERT((Equals<float,  nth<mylist_t, Int<1>>>::Value));
	STATIC_ASSERT((Equals<char,   nth<mylist_t, Int<2>>>::Value));
	STATIC_ASSERT((Equals<double, nth<mylist_t, Int<3>>>::Value));
	STATIC_ASSERT((Length<mylist_t>::Value == 4), "list-length");

	STATIC_ASSERT((predicates::Equals<int>::Apply<int>::Value), "p-eq");
	
	STATIC_ASSERT((index_of<predicates::Equals<int>,    mylist_t>::Value == 0), "index-of");
	STATIC_ASSERT((index_of<predicates::Equals<float>,  mylist_t>::Value == 1), "index-of");
	STATIC_ASSERT((index_of<predicates::Equals<char>,   mylist_t>::Value == 2), "index-of");
	STATIC_ASSERT((index_of<predicates::Equals<double>, mylist_t>::Value == 3), "index-of");

	STATIC_ASSERT((Equals<int,    find<predicates::Equals<int>,    mylist_t>>::Value), "find");
	STATIC_ASSERT((Equals<float,  find<predicates::Equals<float>,  mylist_t>>::Value), "find");
	STATIC_ASSERT((Equals<char,   find<predicates::Equals<char>,   mylist_t>>::Value), "find");
	STATIC_ASSERT((Equals<double, find<predicates::Equals<double>, mylist_t>>::Value), "find");

	STATIC_ASSERT((Equals<VA<int, float, char, double>, to_va<predicates::VAWrapper<VA>, mylist_t>>::Value), "to-va");

	STATIC_ASSERT((Equals<make_list<int, float, char, double>, remove_first<mylist_t, Int<0>>>::Value), "remove-first");
	STATIC_ASSERT((Equals<make_list<float, char, double>, remove_first<mylist_t, Int<1>>>::Value), "remove-first");
	STATIC_ASSERT((Equals<make_list<char, double>, remove_first<mylist_t, Int<2>>>::Value), "remove-first");
	STATIC_ASSERT((Equals<make_list<double>, remove_first<mylist_t, Int<3>>>::Value), "remove-first");
	STATIC_ASSERT((Equals<make_list<>, remove_first<mylist_t, Int<4>>>::Value), "remove-first");

	STATIC_ASSERT((Equals<make_list<int, float, char, double>, remove_last<mylist_t, Int<0>>>::Value), "remove-last");
	STATIC_ASSERT((Equals<make_list<int, float, char>, remove_last<mylist_t, Int<1>>>::Value), "remove-last");
	STATIC_ASSERT((Equals<make_list<int, float>, remove_last<mylist_t, Int<2>>>::Value), "remove-last");
	STATIC_ASSERT((Equals<make_list<int>, remove_last<mylist_t, Int<3>>>::Value), "remove-last");
	STATIC_ASSERT((Equals<make_list<>, remove_last<mylist_t, Int<4>>>::Value), "remove-last");

	STATIC_ASSERT((Equals<make_list<int, float, char, double>, mylist_t>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int, float, char, double>, sub_list<mylist_t, Int<0>, Int<4>>>::Value), "sublist");
    STATIC_ASSERT((Equals<make_list<float, char, double>,      sub_list<mylist_t, Int<1>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<char, double>,             sub_list<mylist_t, Int<2>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<double>,                   sub_list<mylist_t, Int<3>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<>,                         sub_list<mylist_t, Int<4>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int, float, char, double>, sub_list<mylist_t, Int<0>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int, float, char>,         sub_list<mylist_t, Int<0>, Int<3>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int, float>,               sub_list<mylist_t, Int<0>, Int<2>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int>,                      sub_list<mylist_t, Int<0>, Int<1>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<>,                         sub_list<mylist_t, Int<0>, Int<0>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<int, float, char, double>, sub_list<mylist_t, Int<0>, Int<4>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<float, char>,              sub_list<mylist_t, Int<1>, Int<3>>>::Value), "sublist");
	STATIC_ASSERT((Equals<make_list<>,                         sub_list<mylist_t, Int<2>, Int<2>>>::Value), "sublist");

	STATIC_ASSERT(( All<predicates::Identity, make_list<True,  True,  True>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<False, True,  True>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<True,  False, True>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<False, False, True>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<True,  True,  False>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<False, True,  False>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<True,  False, False>>::Value));
	STATIC_ASSERT((!All<predicates::Identity, make_list<False, False, False>>::Value));

	STATIC_ASSERT(( Any<predicates::Identity, make_list<True,  True,  True>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<False, True,  True>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<True,  False, True>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<False, False, True>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<True,  True,  False>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<False, True,  False>>::Value));
	STATIC_ASSERT(( Any<predicates::Identity, make_list<True,  False, False>>::Value));
	STATIC_ASSERT((!Any<predicates::Identity, make_list<False, False, False>>::Value));
}

