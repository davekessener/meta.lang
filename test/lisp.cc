#include "lisp.h"

namespace test
{
	using namespace tml;
	using namespace lisp;

	typedef STR("abc") str_1;
	typedef STR("def") str_2;
	typedef SYMBOL(myvar) sym;
	typedef decimal::to_f<-3,141> num_t;

	STATIC_ASSERT((Equals<STR("ABC"), repl<_<_to_s, _<_quote, SYMBOL(abc)>>>>::Value));
	STATIC_ASSERT((Equals<STR("Closure"), repl<_<_to_s, _<_lambda, _<>, _nil>>>>::Value));

	STATIC_ASSERT((Equals<str_1, eval<str_1, Nil>>::Value), "eval(STR)");
	STATIC_ASSERT((Equals<num_t, eval<num_t, Nil>>::Value), "eval(NUM)");
	STATIC_ASSERT((Equals<True,  eval<True, Nil>>::Value),  "eval(#T)");
	STATIC_ASSERT((Equals<False, eval<False, Nil>>::Value), "eval(#F)");

	STATIC_ASSERT((Equals<STR("(A \"b\" C)"), repl<
		_<_to_s, __<SYMBOL(a), STR("b"), SYMBOL(c)>>
	>>::Value));

	STATIC_ASSERT((Equals<STR("(A . B)"), repl<
		_<_to_s, _<_quote, List<SYMBOL(a), SYMBOL(b)>>>
	>>::Value));

	STATIC_ASSERT((Equals<STR("(A (B C (D . E) F) G (X Y . Z))"), repl<
		_<_to_s, __<
			SYMBOL(a),
			_<SYMBOL(b), SYMBOL(c), List<SYMBOL(d), SYMBOL(e)>, SYMBOL(f)>,
			SYMBOL(g),
			List<SYMBOL(x), List<SYMBOL(y), SYMBOL(z)>>>>
	>>::Value));

	STATIC_ASSERT((Equals<num_t, eval<Symbol<str_1>, List<List<Symbol<str_1>, num_t>, Nil>>>::Value), "eval(SYM)");

	STATIC_ASSERT((Equals<Primitive<fn::Begin>, eval<_begin, make_list<List<_begin, Primitive<fn::Begin>>>>>::Value));
	STATIC_ASSERT((Equals<num_t, eval<make_list<_begin, num_t>, make_list<List<_begin, Primitive<fn::Begin>>>>>::Value));
	STATIC_ASSERT((Equals<num_t, repl<str_1, str_2, num_t>>::Value), "eval(begin)");

	STATIC_ASSERT((repl<_true>::Value));
	STATIC_ASSERT((!repl<_false>::Value));

	STATIC_ASSERT((Equals<str_1, repl<_<_if, _true, str_1, str_2>>>::Value), "a == (if #t a b)");
	STATIC_ASSERT((Equals<str_2, repl<_<_if, _false, str_1, str_2>>>::Value), "b == (if #f a b)");
	STATIC_ASSERT((Equals<Nil, repl<_<_if, _false, str_1>>>::Value), "NIL == (if #f a)");

	STATIC_ASSERT((Equals<str_1, repl<
		_<_if, _<_negativep, to_f<-1,0>>, str_1, str_2>
	>>::Value), "a == (if (negative? -1) a b)");

	STATIC_ASSERT((Equals<str_2, repl<
		_<_if, _<_zerop, to_f<-1,0>>, str_1, str_2>
	>>::Value), "a == (if (zero? -1) a b)");

	STATIC_ASSERT((repl<_<_zerop, to_f<0,0>>>::Value), "(zero? 0)");
	STATIC_ASSERT(!(repl<_<_zerop, to_f<1,0>>>::Value), "(zero? 1)");
	STATIC_ASSERT(!(repl<_<_negativep, to_f<1,0>>>::Value), "(negative? 1)");
	STATIC_ASSERT((repl<_<_negativep, to_f<-1,0>>>::Value), "(negative? -1)");
	STATIC_ASSERT(!(repl<_<_negativep, to_f<0,0>>>::Value), "(negative? 0)");

	STATIC_ASSERT((repl<_<_nilp, _nil>>::Value));
	STATIC_ASSERT(!(repl<_<_nilp, __<str_1, str_2>>>::Value));

	STATIC_ASSERT((Equals<str_1, repl<_<_car, _<_cons, str_1, str_2>>>>::Value));
	STATIC_ASSERT((Equals<str_2, repl<_<_cdr, _<_cons, str_1, str_2>>>>::Value));

	STATIC_ASSERT((Equals<make_list<STR("a"), STR("b"), STR("c")>, repl<
		_<_s_to_list, str_1>
	>>::Value));

	STATIC_ASSERT((Equals<str_1, repl<
		_<_s_append, STR("a"), STR("b"), STR("c")>
	>>::Value));

	STATIC_ASSERT((Equals<STR("ABC"), repl<
		_<_to_s, _<_quote, SYMBOL(abc)>>
	>>::Value));

	STATIC_ASSERT((Equals<SYMBOL(abc), repl<
		_<_s_to_sym, str_1>
	>>::Value));

	STATIC_ASSERT((decimal::Equals<to_f<10,0>, repl<
		_<_add, to_f<1,0>, to_f<2,0>, to_f<3,0>, to_f<4,0>>
	>>::Value), "10 == (+ 1 2 3 4)");

	STATIC_ASSERT((decimal::Equals<to_f<0,0>, repl<
		_<_sub, to_f<10,0>, to_f<1,0>, to_f<2,0>, to_f<3,0>, to_f<4,0>>
	>>::Value), "0 == (- 10 1 2 3 4)");

	STATIC_ASSERT((decimal::Equals<to_f<12,0>, repl<_<_mul, to_f<3,0>, to_f<4,0>>>>::Value), "12 == (* 3 4)");

	STATIC_ASSERT((Equals<num_t, repl<
		_<_define, sym, num_t>,
		sym
	>>::Value), "(begin (define myvar -3.141) myvar)");

	STATIC_ASSERT((Equals<num_t, repl<_<_quote, num_t>>>::Value), "(quote -3.141)");
	STATIC_ASSERT((Equals<make_list<str_1, str_2, num_t>, repl<
		__<str_1, str_2, num_t>
	>>::Value), "'(\"abc\" \"def\" -3.141)");

	STATIC_ASSERT((Equals<num_t, repl<
		_<_define,
			sym,
			_<_lambda,
				_<_x>,
				num_t>>,
		_<sym, str_1>
	>>::Value), "((lambda (x) -3.141) \"abc\")");

	STATIC_ASSERT((Equals<str_1, repl<
		_<_define,
			sym,
			_<_lambda,
				_<_x>,
				_x>>,
		_<sym, str_1>
	>>::Value), "((lambda (x) x) \"abc\")");

	STATIC_ASSERT((Equals<make_list<_add, to_f<1,0>, to_f<2,0>>, repl<
		__<_add, to_f<1,0>, to_f<2,0>>
	>>::Value), "(+ 1 2) == '(+ 1 2)");

	STATIC_ASSERT((decimal::Equals<to_f<3,0>, repl<
		_<_eval, __<_add, to_f<1,0>, to_f<2,0>>>
	>>::Value), "3 == (eval '(+ 1 2))");

	STATIC_ASSERT((decimal::Equals<to_f<120,0>, repl<
		_<_define,
			SYMBOL(fact),
			_<_lambda,
				_<_n>,
				_<_if,
					_<_zerop, _n>,
					to_f<1,0>,
					_<_mul,
						_n,
						_<SYMBOL(fact),
							_<_sub, _n, to_f<1,0>>>>>>>,
		_<SYMBOL(fact), to_f<5,0>>
	>>::Value), "120 == (fact 5)");

	STATIC_ASSERT((decimal::Equals<to_f<120>, repl<
		_<_define, SYMBOL(fact), _<_lambda, _<_n>,
			_<_begin,
				_<_define, SYMBOL(impl), _<_lambda, _<_n>,
					_<_if, _<_zerop, _n>,
						to_f<1>,
						_<_mul,
							_n,
							_<SYMBOL(impl),
								_<_sub, _n, to_f<1>>>>>>>,
				_<SYMBOL(impl), _n>>>>,
		_<_define, SYMBOL(impl), _<_lambda, _<_n>, to_f<1337>>>,
		_<SYMBOL(fact), to_f<5>>
	>>::Value), "no redefining in inner env");

	STATIC_ASSERT((decimal::Equals<to_f<'a',0>, repl<
		_<_ord, STR("abc")>
	>>::Value));
	STATIC_ASSERT((decimal::Equals<to_f<'b',0>, repl<
		_<_ord, STR("abc"), to_f<1,0>>
	>>::Value));
	STATIC_ASSERT((Equals<STR("a"), repl<
		_<_char, to_f<'a',0>>
	>>::Value));

	STATIC_ASSERT((Equals<STR("a"), repl<_<_if, to_f<0>, STR("a"), STR("b")>>>::Value));

	STATIC_ASSERT((Equals<make_list<to_f<'a',0>, to_f<'b',0>, to_f<'c',0>>, repl<
		_<_define, SYMBOL(map), _<_lambda, _<_f, _l>,
			_<_if, _<_nilp, _l>,
				_nil,
				_<_cons,
					_<_f, _<_car, _l>>,
					_<SYMBOL(map), _f, _<_cdr, _l>>>>>>,
		_<SYMBOL(map), _ord, _<_s_to_list, STR("abc")>>
	>>::Value));

	STATIC_ASSERT((Equals<make_list<STR("a"), STR("b"), STR("c")>, repl<
		_<_s_to_list, STR("abc")>
	>>::Value));

	STATIC_ASSERT((Equals<make_list<STR("a"), STR("b"), STR("c")>, repl<
		_<_list, STR("a"), STR("b"), STR("c")>
	>>::Value));

	STATIC_ASSERT((Equals<to_f<3>, repl<
		_<_cond,
			_<_<_eq, to_f<1>, to_f<2>>, to_f<9>>,
			_<_<_lt, to_f<4>, to_f<3>>, STR("what")>,
			_<_<_lt, to_f<1>, to_f<2>>, _<_sub, to_f<10>, to_f<7>>>,
			_<_true, to_f<13>>>
	>>::Value));
	STATIC_ASSERT((Equals<
		_<_begin,
			_<_define, _x, to_f<7>>,
			_<_define, _y, _<_lambda, _<_x>, _x>>,
			_<_mul, _x, _<_y>>>,
		macro_expand<
			_<_let, _<
				_<_x, to_f<7>>,
				_<_y, _<_lambda, _<_x>, _x>>
			>,
				_<_mul, _x, _<_y>>>,
			user_lib_t
		>
	>::Value));
	STATIC_ASSERT((Equals<
		_<_begin,
			_<_define, _x, to_f<7>>,
			_<_define, _y, _<_lambda, _<_x>, _x>>,
			_<_mul, _x, _<_y>>>,
		eval<
			_<_expand_macro,
				_<_let, _<
					_<_x, to_f<7>>,
					_<_y, _<_lambda, _<_x>, _x>>
				>,
					_<_mul, _x, _<_y>>>>,
			user_lib_t
		>
	>::Value));

	STATIC_ASSERT((Equals<STR("b"), repl<
		_<_cdr,
			_<_find,
				_<_lambda, _<_e>,
					_<_eq, _<_car, _e>, _<_quote, SYMBOL(y)>>>,
				__<
					List<SYMBOL(x), STR("a")>,
					List<SYMBOL(y), STR("b")>,
					List<SYMBOL(z), STR("c")>
				>>>
	>>::Value));

	typedef make_list<
		List<SYMBOL(f-lib), repl<
			_<_define, SYMBOL(impl), _<_lambda, _<_n>,
				_<_if, _<_zerop, _n>,
					to_f<1>,
					_<_mul,
						_n,
						_<SYMBOL(impl), _<_sub, _n, to_f<1>>>>>>>,
			_<_export,
				_<SYMBOL(fact), SYMBOL(impl)>>
		>>
	> fact_lib_t;

	template<typename>
	struct is_lambda : False
	{
	};

	template<typename P, typename B, typename X>
	struct is_lambda<Closure<P, B, X>> : True
	{
	};

	template<typename T, typename N>
	struct is_lambda<LateBinding<T, N>> : is_lambda<T>
	{
	};

	STATIC_ASSERT((Equals<ListType, type_of<cdr<car<fact_lib_t>>>>::Value));
	STATIC_ASSERT((1 == Length<cdr<car<fact_lib_t>>>::Value));
	STATIC_ASSERT((Equals<SYMBOL(fact), car<car<cdr<car<fact_lib_t>>>>>::Value));
	STATIC_ASSERT((is_lambda<cdr<car<cdr<car<fact_lib_t>>>>>::Value));

	STATIC_ASSERT((Equals<cdr<car<fact_lib_t>>, Repl<fact_lib_t>::Run<_<_eval, _<_quote, SYMBOL(f-lib)>>>::Type>::Value));

	STATIC_ASSERT((decimal::Equals<to_f<24>, Repl<fact_lib_t>::Run<
		_<_import, SYMBOL(f-lib), SYMBOL(fact)>,
		_<SYMBOL(fact), to_f<4>>
	>::Type>::Value));

	STATIC_ASSERT((Equals<STR("BOOL"),    repl<_<_type_of, _false>>>::Value));
	STATIC_ASSERT((Equals<STR("NUMBER"),  repl<_<_type_of, to_f<1,0>>>>::Value));
	STATIC_ASSERT((Equals<STR("LIST"),    repl<_<_type_of, _nil>>>::Value));
	STATIC_ASSERT((Equals<STR("CLOSURE"), repl<_<_type_of, _<_lambda, _<>, _nil>>>>::Value));
	STATIC_ASSERT((Equals<STR("STRING"),  repl<_<_type_of, STR("abc")>>>::Value));
	STATIC_ASSERT((Equals<STR("SYMBOL"),  repl<_<_type_of, _<_quote, _lambda>>>>::Value));
	STATIC_ASSERT((Equals<STR("MACRO"),   repl<_<_type_of, _<_macro, _l, _nil>>>>::Value));
	STATIC_ASSERT((Equals<STR("BOOL"),    repl<_<_type_of, _<_eq, to_f<1,0>, STR("ABC")>>>>::Value));
	STATIC_ASSERT((Equals<STR("CLOSURE"), repl<
		_<_define, SYMBOL(testfn), _<_lambda, _<_x>,
			_<_mul, _x, _x>>>,
		_<_type_of, SYMBOL(testfn)>
	>>::Value));
}

