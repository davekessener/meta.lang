#include "string.h"

namespace test
{
	using namespace tml;

	typedef STR("Hello, World!") mystr_t;

	STATIC_ASSERT((string_length<mystr_t>::Value == 14), "string-length");
	STATIC_ASSERT((string_ref<mystr_t, Int<0>>::Value == 'H'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<1>>::Value == 'e'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<2>>::Value == 'l'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<3>>::Value == 'l'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<4>>::Value == 'o'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<5>>::Value == ','), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<6>>::Value == ' '), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<7>>::Value == 'W'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<8>>::Value == 'o'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<9>>::Value == 'r'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<10>>::Value == 'l'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<11>>::Value == 'd'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<12>>::Value == '!'), "string-ref");
	STATIC_ASSERT((string_ref<mystr_t, Int<13>>::Value == 0), "string-ref");

	STATIC_ASSERT((Equals<STR("ABCDEF"), to_upper<STR("aBcDeF")>>::Value));
	STATIC_ASSERT((Equals<STR("abcdef"), to_lower<STR("AbCdEf")>>::Value));
}

