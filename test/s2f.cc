#include "s2f.h"

namespace test
{
	using namespace tml;
	using decimal::to_f;

	STATIC_ASSERT((decimal::Equals<to_f<-123,4567>, FLOAT(-123.4567)>::Value), "s2f");
	STATIC_ASSERT((Equals<STR("-123.125"), f_to_s<to_f<-123,125>>>::Value), "f2s");
}

