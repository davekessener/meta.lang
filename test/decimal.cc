#include "decimal.h"

namespace test
{
	using namespace tml;
	using namespace decimal;

	struct Squarer
	{
		template<typename V>
		struct Apply
		{
			typedef Mul<V, V> Type;
		};
	};

	STATIC_ASSERT((tml::Equals<
		make_list<to_f<1,0>, to_f<4,0>, to_f<9,0>, to_f<16,0>>,
		map<Squarer, make_list<to_f<1,0>, to_f<2,0>, to_f<3,0>, to_f<4,0>>>
	>::Value), "(map (lambda (x) (* x x)) '(1 2 3 4)) == '(1 4 9 16)");

	struct Adder
	{
		template<typename U, typename V>
		struct Apply
		{
			typedef decimal::Add<U, V> Type;
		};
	};

	STATIC_ASSERT((decimal::Equals<to_f<10,0>, reduce<Adder, make_list<to_f<1,0>, to_f<2,0>, to_f<3,0>, to_f<4,0>>>>::Value));

	STATIC_ASSERT((decimal::Equals<to_f<-123,456>, Mul<to_f<-1,0>, to_f<123,456>>>::Value));

	STATIC_ASSERT((decimal::Equals<to_f<12,0>, Mul<to_f<3,0>, to_f<4,0>>>::Value), "12.0 == 3.0 * 4.0");
	STATIC_ASSERT((decimal::Equals<to_f<0,125>, Div<to_f<1,0>, to_f<8,0>>>::Value), "0.125 == 1.0 / 8.0");
	STATIC_ASSERT((decimal::IsZero<Mul<to_f<0,0>, constant::PI>>::Value), "0 == 0 * PI");

	STATIC_ASSERT((decimal::LessThan<to_f<0,0>, to_f<1,0>>::Value));
	STATIC_ASSERT((decimal::LessThan<to_f<0,5>, to_f<1,5>>::Value));
	STATIC_ASSERT((decimal::LessThan<to_f<0,125>, to_f<0,1875>>::Value));
	STATIC_ASSERT((decimal::GreaterThan<to_f<1,0>, to_f<0,0>>::Value));
	STATIC_ASSERT((decimal::GreaterThan<to_f<1,5>, to_f<0,5>>::Value));
	STATIC_ASSERT((decimal::GreaterThan<to_f<0,1875>, to_f<0,125>>::Value));

	STATIC_ASSERT((decimal::GreaterThan<to_f<0,0>, to_f<-1,0>>::Value));
	STATIC_ASSERT((decimal::GreaterThan<to_f<0,5>, to_f<-1,5>>::Value));
	STATIC_ASSERT((decimal::GreaterThan<to_f<0,125>, Mul<to_f<-1,0>, to_f<0,1875>>>::Value));
	STATIC_ASSERT((decimal::LessThan<to_f<-1,0>, to_f<0,0>>::Value));
	STATIC_ASSERT((decimal::LessThan<to_f<-1,5>, to_f<0,5>>::Value));
	STATIC_ASSERT((decimal::LessThan<Mul<to_f<-1,0>, to_f<0,1875>>, to_f<0,125>>::Value));

	STATIC_ASSERT((3 == to_i<constant::PI>::Value));
	STATIC_ASSERT((3 == to_i<to_f<3,0>>::Value));
	STATIC_ASSERT((3 == to_i<to_f<3,1>>::Value));
	STATIC_ASSERT((3 == to_i<to_f<3,5>>::Value));
	STATIC_ASSERT((3 == to_i<to_f<3,9>>::Value));
	STATIC_ASSERT((4 == to_i<to_f<4,0>>::Value));
	STATIC_ASSERT((-3 == to_i<to_f<-3,0>>::Value));
	STATIC_ASSERT((-3 == to_i<to_f<-3,1>>::Value));
	STATIC_ASSERT((-3 == to_i<to_f<-3,5>>::Value));
	STATIC_ASSERT((-3 == to_i<to_f<-3,9>>::Value));
	STATIC_ASSERT((-4 == to_i<to_f<-4,0>>::Value));
}

