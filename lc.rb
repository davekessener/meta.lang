#!/usr/bin/env ruby
#
@lines = {}
Dir.glob('**/*.{h,c,cc,cl,rb,sh}').each do |fn|
	@lines[fn] = File.read(fn).split(/\n/).length
end
l = @lines.map { |fn, c| fn.length }.max
t = @lines.map { |fn, c| c }.sum
@lines.to_a.sort { |a, b| b[1] - a[1] }.each do |fn, c|
	puts "#{"%-#{l}s" % fn} #{c}"
end
puts '-' * (l + 7)
puts "#{"%-#{l}s" % 'total'} #{t}"

