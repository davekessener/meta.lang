#ifndef META_DECIMAL_H
#define META_DECIMAL_H

#include <string>
#include <iomanip>

#include "util.h"
#include "tml.h"

namespace decimal {

using tml::True;
using tml::False;
using tml::Bool;
using tml::If;
using tml::Type2Type;

namespace detail {

template<uint32_t V>
struct Base
{
	static constexpr uint64_t Value = V;
};

template<typename A, typename B>
struct Compound
{
	typedef A High;
	typedef B Low;
};

template<typename, uint32_t>
struct Clone;

template<typename>
struct Invert;

template<typename, typename, uint64_t>
struct Add;

template<typename T>
struct Negate
{
	typedef typename Invert<T>::Type i;
	typedef typename Clone<T, 0>::Type a;
	typedef typename Add<i, a, 1>::Type Type;
};

template<typename H, typename L, uint32_t V>
struct Clone<Compound<H, L>, V>
{
	typedef Compound<
		typename Clone<H, 0>::Type,
		typename Clone<L, V>::Type
	> Type;
};

template<uint32_t O, uint32_t V>
struct Clone<Base<O>, V>
{
	typedef Base<V> Type;
};

template<typename AH, typename AL, typename BH, typename BL, uint64_t C>
struct Add<Compound<AH, AL>, Compound<BH, BL>, C>
{
	typedef Add<AL, BL, C> tl;
	typedef Add<AH, BH, tl::Carry> th;

	typedef Compound<typename th::Type, typename tl::Type> Type;
	static constexpr uint64_t Carry = th::Carry;
};

template<uint32_t A, uint32_t B, uint64_t C>
struct Add<Base<A>, Base<B>, C>
{
	static constexpr uint64_t t = C + (uint64_t) B + (uint64_t) A;

	typedef Base<((uint32_t) t)> Type;
	static constexpr uint64_t Carry = (t >> 32);
};

template<typename H, typename L>
struct Invert<Compound<H, L>>
{
	typedef Compound<
		typename Invert<H>::Type,
		typename Invert<L>::Type
	> Type;
};

template<uint32_t V>
struct Invert<Base<V>>
{
	typedef Base<(~V)> Type;
};

template<typename, typename>
struct Mul;

template<typename AH, typename AL, typename BH, typename BL>
struct Mul<Compound<AH, AL>, Compound<BH, BL>>
{
	typedef Mul<AL, BL> t00;
	typedef Mul<AL, BH> t01;
	typedef Mul<AH, BL> t10;
	typedef Mul<AH, BH> t11;

	typedef typename t00::Type r1;

	typedef Add<
		typename t01::Type,
		typename t10::Type,
		0
	> p_1;
	typedef Add<
		typename t00::Carry,
		typename p_1::Type,
		0
	> p_2;

	typedef typename p_2::Type r2;

	typedef Add<
		typename t11::Type,
		typename t01::Carry,
		p_1::Carry
	> p_3;
	typedef Add<
		typename t10::Carry,
		typename p_3::Type,
		p_2::Carry
	> p_4;

	typedef Add<
		typename t11::Carry,
		typename Clone<typename t11::Carry, 0>::Type,
		(p_3::Carry + p_4::Carry)
	> p_5;

	typedef Compound<
		typename p_2::Type,
		typename t00::Type
	> Type;
	typedef Compound<
		typename p_5::Type,
		typename p_4::Type
	> Carry;

	STATIC_ASSERT(!(p_5::Carry));
};

template<uint32_t A, uint32_t B>
struct Mul<Base<A>, Base<B>>
{
	static constexpr uint64_t v = A * (uint64_t) B;
	typedef Base<(uint32_t) v> Type;
	typedef Base<(v >> 32)> Carry;
};

template<typename, uint32_t, int>
struct ShlImpl;

template<typename H, typename L, uint32_t V, int N>
struct ShlImpl<Compound<H, L>, V, N>
{
	typedef ShlImpl<L, V, N> tl;
	typedef ShlImpl<H, tl::Carry, N> th;

	typedef Compound<
		typename th::Type,
		typename tl::Type
	> Type;

	static constexpr uint32_t Carry = th::Carry;
};

template<uint32_t V, uint32_t P, int N>
struct ShlImpl<Base<V>, P, N>
{
	STATIC_ASSERT((N >= 0 && N <= 31));

	static constexpr uint64_t w = V;
	typedef Base<(uint32_t) ((w << N) | P)> Type;
	static constexpr uint32_t Carry = (uint32_t) (w >> (32 - N));
};

template<typename V, uint32_t P, int N, bool OV>
struct ShlPart : ShlImpl<V, P, N>
{
};

template<typename V, uint32_t P, int N>
struct ShlPart<V, P, N, true>
	: ShlPart<
		typename ShlImpl<V, 0, 31>::Type,
		P,
		(N - 31),
		((N - 31) > 31)
	>
{
};

template<typename V, uint32_t P, int N>
struct Shl : ShlPart<V, P, N, (N > 31)>
{
};

template<typename, uint32_t, int>
struct ShrImpl;

template<typename H, typename L, uint32_t V, int N>
struct ShrImpl<Compound<H, L>, V, N>
{
	typedef ShrImpl<H, V, N> th;
	typedef ShrImpl<L, th::Carry, N> tl;

	typedef Compound<
		typename th::Type,
		typename tl::Type
	> Type;

	static constexpr uint32_t Carry = tl::Carry;
};

template<uint32_t V, uint32_t P, int N>
struct ShrImpl<Base<V>, P, N>
{
	STATIC_ASSERT((N >= 0 && N <= 31));

	static constexpr uint64_t w = V;
	typedef Base<(P | (w >> N))> Type;
	static constexpr uint32_t Carry = (uint32_t) (w << (32 - N));
};

template<typename V, uint32_t P, int N, bool OV>
struct ShrPart : ShrImpl<V, P, N>
{
};

template<typename V, uint32_t P, int N>
struct ShrPart<V, P, N, true>
	: ShrPart<
		typename ShrImpl<V, 0, 31>::Type,
		P,
		(N - 31),
		((N - 31) > 31)
	>
{
};

template<typename V, uint32_t P, int N>
struct Shr : ShrPart<V, P, N, (N > 31)>
{
};

template<typename>
struct IsZero;

template<typename H, typename L>
struct IsZero<Compound<H, L>>
{
	static constexpr bool Value = ((IsZero<H>::Value) && (IsZero<L>::Value));
};

template<uint32_t V>
struct IsZero<Base<V>>
{
	static constexpr bool Value = !V;
};

template<typename, typename>
struct LessThan;

template<typename AH, typename AL, typename BH, typename BL>
struct LessThan<Compound<AH, AL>, Compound<BH, BL>>
{
	static constexpr bool lt = LessThan<AH, BH>::Value;
	static constexpr bool gt = LessThan<BH, AH>::Value;
	static constexpr bool Value = (lt || (!gt && LessThan<AL, BL>::Value));
};

template<uint32_t A, uint32_t B>
struct LessThan<Base<A>, Base<B>>
{
	static constexpr bool Value = (A < B);
};

template<typename>
struct BitWidth;

template<typename H, typename L>
struct BitWidth<Compound<H, L>>
{
	static constexpr int Value = BitWidth<H>::Value + BitWidth<L>::Value;
};

template<uint32_t V>
struct BitWidth<Base<V>>
{
	static constexpr int Value = 32;
};

template<typename>
struct LeadingZeroesPart; // LeadingZeroesPart<00000101>::Value == 3 [00000  101] [5 3]

template<typename H, typename L>
struct LeadingZeroesPart<Compound<H, L>>
{
	typedef LeadingZeroesPart<H> t1;
	typedef LeadingZeroesPart<L> t2;
	static constexpr int Width = t1::Width + t2::Width;
	static constexpr int Value = (t1::Value ? (t2::Width + t1::Value) : t2::Value);
};

template<uint32_t V>
struct LeadingZeroesPart<Base<V>>
{
	static constexpr int Width = 32;
	static constexpr int Value = 1 + LeadingZeroesPart<Base<(V >> 1)>>::Value;
};

template<>
struct LeadingZeroesPart<Base<0>>
{
	static constexpr int Width = 32;
	static constexpr int Value = 0;
};

template<typename V>
struct LeadingZeroes
{
	typedef LeadingZeroesPart<V> t;
	static constexpr int Value = t::Width - t::Value;
};

template<typename A, typename B, typename Q, int I>
struct DivImpl
{
	STATIC_ASSERT((!IsZero<B>::Value));

	static constexpr bool gte = !LessThan<A, B>::Value;
	typedef typename Shl<Q, (gte ? 1 : 0), 1>::Type nq;
	typedef typename If<
		Bool<gte>,
		Add<A, typename Negate<B>::Type, 0>,
		Type2Type<A>
	>::Type na;
	typedef typename Shr<B, 0, 1>::Type nb;

	typedef typename DivImpl<na, nb, nq, (I - 1)>::Type Type;
};

template<typename A, typename B, typename Q>
struct DivImpl<A, B, Q, 0>
{
	static constexpr int Width = BitWidth<Q>::Value;
	typedef A Remainder;
	typedef Q Quotient;
	typedef typename Add<
		Compound<Remainder, typename Clone<Remainder, 0>::Type>,
		Compound<typename Clone<Quotient, 0>::Type, Quotient>,
		0
	>::Type Type;
};

template<typename A, typename B, bool>
struct DivPart
{
	static constexpr int Width = BitWidth<A>::Value;

	static constexpr int shft = LeadingZeroes<B>::Value - LeadingZeroes<A>::Value;
	typedef typename Shl<B, 0, shft>::Type divisor;

	typedef typename DivImpl<
		A,
		divisor,
		typename Clone<A, 0>::Type,
		(shft + 1)
	>::Type Type;
};

template<typename A, typename B>
struct DivPart<A, B, true>
{
	typedef Compound<A, typename Clone<A, 0>::Type> Type;
};

template<typename A, typename B>
struct Div : DivPart<A, B, LessThan<A, B>::Value>
{
	STATIC_ASSERT((!IsZero<B>::Value));
};

template<typename A, typename B>
using add = typename Add<A, B, 0>::Type;

template<typename A, typename B>
using sub = typename Add<A, typename Negate<B>::Type, 0>::Type;

template<typename A, typename B>
using mul = typename Mul<A, B>::Type;

template<typename A, typename B>
using div = typename Div<A, B>::Type;

template<typename A, typename B>
struct Equals : Bool<(!LessThan<A, B>::Value && !LessThan<B, A>::Value)>
{
};

template<typename A, typename B>
struct GreaterThan : LessThan<B, A>
{
};

template<typename A, typename B>
struct LessThanOrEqual : Bool<(!LessThan<B, A>::Value)>
{
};

template<typename A, typename B>
struct GreaterThanOrEqual : Bool<(!LessThan<A, B>::Value)>
{
};

template<typename V, int N, uint32_t P = 0>
using shl = typename Shl<V, P, N>::Type;

template<typename V, int N, uint32_t P = 0>
using shr = typename Shr<V, P, N>::Type;

template<typename V>
using inc = add<V, typename Clone<V, 1>::Type>;

template<typename V>
using dec = sub<V, typename Clone<V, 1>::Type>;


template<typename>
struct Printer;

template<typename A, typename B>
struct Printer<Compound<A, B>>
{
	static std::string to_s(void)
	{
		return Printer<A>::to_s() + ":" + Printer<B>::to_s();
	}
};

template<uint32_t V>
struct Printer<Base<V>>
{
	static std::string to_s(void)
	{
		std::stringstream ss;
		ss << std::hex << std::setfill('0') << std::setw(8) << V;
		return ss.str();
	}
};

}

// # ===============================================================================
// # --- Float ---------------------------------------------------------------------
// # ===============================================================================
// # --- Simplified implementation of IEEE Floating Point standard               ---
// # --- Uses a 64bit Mantissa and a 32bit exponent                              ---
// # ===============================================================================

typedef detail::Compound<
	detail::Compound<
		detail::Base<0>,
		detail::Base<0>
	>,
	detail::Compound<
		detail::Base<(-1)>,
		detail::Base<(-1)>
	>
> T_MAX;
typedef detail::inc<detail::shr<T_MAX,1>> T_MIN;
typedef detail::Clone<T_MAX,0>::Type T_ZERO;
typedef detail::Clone<T_MAX::Low,0>::Type M_ZERO;
typedef detail::Base<0> E_ZERO;

template<bool S, typename E, typename M>
struct Float
{
	STATIC_ASSERT((detail::BitWidth<M>::Value == detail::BitWidth<M_ZERO>::Value));
	STATIC_ASSERT((!detail::Equals<M, M_ZERO>::Value || (S && detail::Equals<E, E_ZERO>::Value)));
	STATIC_ASSERT((detail::BitWidth<E>::Value == detail::BitWidth<E_ZERO>::Value));

	static constexpr bool Sign = S;
	typedef E Exponent;
	typedef M Mantissa;
};

template<bool SS, typename EE, typename MM>
struct MakeFloat
{
	STATIC_ASSERT((detail::BitWidth<MM>::Value == detail::BitWidth<T_MAX>::Value));
	STATIC_ASSERT((detail::BitWidth<MM>::Value == detail::BitWidth<T_MIN>::Value));
	STATIC_ASSERT((detail::BitWidth<MM>::Value == 128));
	STATIC_ASSERT((detail::BitWidth<EE>::Value == 32));

	template<int, bool, typename, typename>
	struct impl;

	template<bool S, typename E, typename M>
	struct impl<-1, S, E, M>
	{
		typedef detail::shl<M,1> shifted;
		static constexpr int R = (detail::LessThan<shifted, T_MIN>::Value ? -1 : 0);

		typedef typename impl<R, S, detail::dec<E>, shifted>::Type Type;
	};

	template<bool S, typename E, typename M>
	struct impl<1, S, E, M>
	{
		typedef detail::shr<M, 1> shifted;
		static constexpr int R = (detail::GreaterThan<shifted, T_MAX>::Value ? 1 : 0);

		typedef typename impl<R, S, detail::inc<E>, shifted>::Type Type;
	};

	template<bool S, typename E, typename M>
	struct impl<0, S, E, M>
	{
		STATIC_ASSERT((detail::LessThanOrEqual<M, T_MAX>::Value));

		typedef Float<S, E, typename M::Low> Type;
	};

	static constexpr int start = (detail::LessThan<MM, T_MIN>::Value ? -1 : (detail::GreaterThan<MM, T_MAX>::Value ? 1 : 0));

	typedef typename impl<start, SS, EE, MM>::Type Type;
};

template<bool S, typename E>
struct MakeFloat<S, E, T_ZERO>
{
	typedef Float<true, E_ZERO, M_ZERO> Type;
};

template<typename M, typename E = E_ZERO, bool S = true>
using Normalize = typename MakeFloat<S, E, M>::Type;

typedef Float<true, E_ZERO, M_ZERO> ZERO;

// # --------------------------------------------------------------------------------

template<typename>
struct IsZero : False
{
};

template<>
struct IsZero<ZERO> : True
{
};

template<typename F>
struct IsNegative
{
	static constexpr bool Value = !F::Sign;
};

template<typename>
struct IsFloat : False
{
};

template<bool S, typename E, typename M>
struct IsFloat<Float<S, E, M>> : True
{
};

template<typename, typename>
struct LessThan;

template<typename A, typename B>
struct Equals : Bool<(!LessThan<A, B>::Value && !LessThan<B, A>::Value)>
{
};

template<typename A, typename B>
struct GreaterThan : LessThan<B, A>
{
};

template<typename A, typename B>
struct LessThanOrEqual : Bool<(!LessThan<B, A>::Value)>
{
};

template<typename A, typename B>
struct GreaterThanOrEqual : Bool<(!LessThan<A, B>::Value)>
{
};

template<bool S, typename E1, typename M1, typename E2, typename M2>
struct LessThan<Float<S, E1, M1>, Float<S, E2, M2>>
{
	static constexpr bool z1 = IsZero<Float<S, E1, M1>>::Value;
	static constexpr bool z2 = IsZero<Float<S, E2, M2>>::Value;
	static constexpr bool lt = detail::LessThan<E1, E2>::Value;
	static constexpr bool gt = detail::LessThan<E2, E1>::Value;

	static constexpr bool Value = 
		(!z1 && !z2)
			? (lt || (!gt && detail::LessThan<M1, M2>::Value))
			: (z1 && !z2);
};

template<bool S1, typename E1, typename M1, bool S2, typename E2, typename M2>
struct LessThan<Float<S1, E1, M1>, Float<S2, E2, M2>>
{
	static constexpr bool z1 = IsZero<Float<S1, E1, M1>>::Value;
	static constexpr bool z2 = IsZero<Float<S2, E2, M2>>::Value;
	static constexpr bool Value = 
		(!z1 && !z2)
			? !S1
			: (    (z1 && !z2 && S2)
				|| (!z1 && z2 && !S1));
};

// # --------------------------------------------------------------------------------

template<typename A, typename B>
struct DoMul
{
	typedef detail::Mul<typename A::Mantissa, typename B::Mantissa> m;
	typedef detail::add<typename A::Exponent, typename B::Exponent> e;
	static constexpr bool s = (A::Sign == B::Sign);

	typedef Normalize<detail::Compound<typename m::Carry, typename m::Type>, e, s> Type;
};

template<typename A, typename B>
struct DoDiv
{
	typedef detail::Compound<
		typename A::Mantissa,
		typename detail::Clone<
			typename A::Mantissa,
			0
		>::Type
	> m1;
	typedef detail::Compound<
		typename detail::Clone<
			typename B::Mantissa,
			0
		>::Type,
		typename B::Mantissa
	> m2;
	typedef detail::div<m1, m2> m;
	typedef detail::sub<
		detail::sub<
			typename A::Exponent,
			typename B::Exponent
		>,
		typename detail::Clone<
			typename A::Exponent,
			detail::BitWidth<typename A::Mantissa>::Value
		>::Type
	> e;
	static constexpr bool s = (A::Sign == B::Sign);

	typedef Normalize<typename m::Low, e, s> Type;
};

// # --------------------------------------------------------------------------------

template<typename, typename>
struct DoAdd;

template<typename, typename>
struct DoSub;

template<bool C, typename M, typename E, bool S>
struct AddAdj
{
	typedef Normalize<
		detail::Compound<
			typename detail::Clone<M,0>::Type,
			M
		>,
		E,
		S
	> Type;
};

template<typename M, typename E, bool S>
struct AddAdj<true, M, E, S>
{
	typedef typename detail::Shr<M, 0x80000000, 1>::Type m;
	typedef detail::inc<E> e;

	typedef Normalize<detail::Compound<
		typename detail::Clone<m, 0>::Type,
		m
	>, e, S> Type;
};

template<typename, typename>
struct AddImpl;

template<bool S, typename M1, typename E1, typename M2, typename E2>
struct AddImpl<Float<S, E1, M1>, Float<S, E2, M2>>
{
	typedef detail::sub<E1, E2> eo;
	typedef detail::shr<M2, eo::Value> ba;
	typedef detail::Add<M1, ba, 0> r;
	static constexpr bool c = r::Carry;
	typedef typename r::Type m;

	typedef typename AddAdj<c, m, E1, S>::Type Type;
};

template<typename A, typename B, bool>
struct AddPart : AddImpl<A, B>
{
};

template<typename A, typename B>
struct AddPart<A, B, true> : AddImpl<B, A>
{
};

// # --------------------------------------------------------------------------------

template<typename, typename>
struct SubImpl;

template<bool S, typename M1, typename E1, typename M2, typename E2>
struct SubImpl<Float<S, E1, M1>, Float<S, E2, M2>>
{
	typedef detail::sub<E1, E2> eo;
	typedef detail::shr<M2, eo::Value> ba;
	typedef detail::sub<M1, ba> m;

	typedef Normalize<detail::Compound<
		typename detail::Clone<m, 0>::Type,
		m
	>, E1, S> Type;
};

template<typename A, typename B, bool>
struct SubPart : SubImpl<A, B>
{
};

template<typename A, typename B>
struct SubPart<A, B, true> : SubImpl<B, A>
{
};

// # --------------------------------------------------------------------------------

template<bool S, typename M1, typename E1, typename M2, typename E2>
struct DoAdd<Float<S, E1, M1>, Float<S, E2, M2>>
	: AddPart<
		Float<S, E1, M1>,
		Float<S, E2, M2>,
		LessThan<
			Float<S, E1, M1>,
			Float<S, E2, M2>
		>::Value
	>
{
};

template<bool S1, typename M1, typename E1, bool S2, typename M2, typename E2>
struct DoAdd<Float<S1, E1, M1>, Float<S2, E2, M2>>
	: DoSub<Float<S1, E1, M1>, Float<S1, E2, M2>>
{
};

template<bool S, typename E1, typename M1, typename E2, typename M2>
struct DoSub<Float<S, E1, M1>, Float<S, E2, M2>>
	: SubPart<
		Float<S, E1, M1>,
		Float<S, E2, M2>,
		LessThan<
			Float<S, E1, M1>,
			Float<S, E2, M2>
		>::Value
	>
{
};

template<bool S1, typename M1, typename E1, bool S2, typename M2, typename E2>
struct DoSub<Float<S1, E1, M1>, Float<S2, E2, M2>>
	: DoAdd<Float<S1, E1, M1>, Float<S1, E2, M2>>
{
};

template<typename A, typename B>
struct AddWrap : DoAdd<A, B>
{
};

template<typename V>
struct AddWrap<V, ZERO>
{
	typedef V Type;
};

template<typename V>
struct AddWrap<ZERO, V>
{
	typedef V Type;
};

template<>
struct AddWrap<ZERO, ZERO>
{
	typedef ZERO Type;
};

template<typename A, typename B>
struct SubWrap : DoSub<A, B>
{
};

template<typename V>
struct SubWrap<V, ZERO>
{
	typedef V Type;
};

template<bool S, typename E, typename M>
struct SubWrap<ZERO, Float<S, E, M>>
{
	typedef Float<!S, E, M> Type;
};

template<>
struct SubWrap<ZERO, ZERO>
{
	typedef ZERO Type;
};

// # --------------------------------------------------------------------------------

template<typename A, typename B>
using Add = typename AddWrap<A, B>::Type;

template<typename A, typename B>
using Sub = typename SubWrap<A, B>::Type;

template<typename A, typename B>
using Mul = typename DoMul<A, B>::Type;

template<typename A, typename B>
using Div = typename DoDiv<A, B>::Type;

// # -----------------------------------------------------------------------

template<typename>
struct Float2Int;

template<bool S, typename E, typename M>
struct Float2Int<Float<S, E, M>>
{
	static constexpr int32_t e = E::Value;
	static constexpr uint64_t m = (((uint64_t) M::High::Value) << 32) | M::Low::Value;

	STATIC_ASSERT((e <= 0));

	static constexpr int64_t Value = ((-e >= 64) ? 0 : ((S ? 1 : -1) * (m >> -e)));
};

constexpr uint64_t find_div(uint64_t v) { return v ? 10 * find_div(v / 10) : 1; }

template<uint64_t V>
using mantissa = detail::Compound<
	M_ZERO,
	detail::Compound<
		detail::Base<(uint32_t) (V >> 32)>,
		detail::Base<(uint32_t) V>
	>
>;

template<typename F>
using to_i = Float2Int<F>;

template<int64_t I, uint64_t F = 0>
using to_f = Add<
	Normalize<mantissa<(uint64_t) (I < 0 ? -I : I)>, E_ZERO, (I >= 0)>,
	Div<
		Normalize<mantissa<F>, E_ZERO, (I >= 0)>,
		Normalize<mantissa<find_div(F)>>
	>
>;

// # ------------------------------------------------------------

template<typename> struct Evaluator;

template<bool S, typename E, typename M>
struct Evaluator<Float<S, E, M>>
{
	static constexpr int32_t e = E::Value;
	static constexpr uint64_t m = (((uint64_t) M::High::Value) << 32) | M::Low::Value;

	static double evaluate(void)
	{
		double q = 0.0;
		auto mm = m;
		auto ee = e;

		if(ee > 0)
		{
			mm <<= ee;
		}
		else for(; ee < 0 ; ++ee)
		{
			q *= 0.5;
			if(mm & 1) q += 0.5;
			mm >>= 1;
		}

		q += mm;

		return S ? q : -q;
	}
};

template<typename F>
double get(void)
{
	return Evaluator<F>::evaluate();
}

// # ------------------------------------------------------------

namespace constant
{
	typedef to_f<3,1415926535> PI;
	typedef to_f<2,7182818284> E;
}


}

#endif

