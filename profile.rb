require 'fileutils'

class Collector
	def initialize(f)
		@freq = f
		@log = []
		@running = true
		@thread = Thread.new { run }
	end

	def get
		raise unless @running

		@running = false
		@thread.join
		@log
	end

	private

	def run
		while @running
			@log.push(get_used)
			sleep @freq
		end
	end
	
	def get_used
		`free`.split(/\n/)[1..2].map{|l|l.split(/[ \t]+/)[2].to_i}.sum
	end
end

unless ARGV.empty?
	c = Collector.new(1)
	`#{ARGV.join(' ')}`
	r = c.get

	puts "ran for #{r.length}s using at most #{r.max}B"
end

