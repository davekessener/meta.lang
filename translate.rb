#!/usr/bin/env ruby


def puts(*a)
	STDOUT.puts *a
	STDOUT.flush
end

class Worker
	def initialize(&block)
		@queue = []
		@running = true
		@callback = block

		@thread = Thread.new { run }
	end

	def working?
		@activity
	end

	def schedule(&a)
		raise unless @running

		@queue.push(a)
	end

	def stop
		@running = false
		@thread.join
	end

	private

	def run
		while @running
			if @queue.empty?
				sleep 0.01
			else
				@activity = @queue.shift
				r = @activity.call
				@activity = nil
				@callback.call(r)
			end
		end
	end
end

class Scheduler
	def initialize(n, &block)
		@workers = Array.new(n) { Worker.new { |r| handle(r) } }
		@callback = block
		@idx, @expected = 0, 0
		@backlog = []
	end

	def schedule(&cb)
		i = @idx
		@idx += 1
		a = Proc.new { [i, cb.call] }

		if (w = @workers.find { |w| not w.working? })
			w.schedule(&a)
		else
			@workers.sample.schedule(&a)
		end
	end

	def stop
		sleep 0.1 while @idx > @expected
		@workers.each(&:stop)
	end

	private

	def handle(r)
		i, v = *r
		if i == @expected
			@callback.call(v)
			@expected += 1
		else
			@backlog.push(r)
		end

		@backlog.sort! { |a, b| a[0] - b[0] }

		if not @backlog.empty? and @backlog[0][0] == @expected
			handle(@backlog.shift)
		end
	end
end

class Processor
	ID = '[a-zA-Z_][a-zA-Z0-9_-]*'

	def self.run(line)
		line.gsub!(/(#{ID}::)*/, '')
		line.gsub!(/String<(Char<'([^']*)'>,?[ \t]+?)+>/) do |s|
			s.gsub(/Char<'([^']*)'>,?[ \t]+/, '\1').sub(/^String<(.*)>$/, 'String<"\1">')
		end
		line.gsub!(/> /) { |s| '>' }
		line
	end
end

def setup(line)
	v = line.dup
	Proc.new { Processor.run(v) }
end

@service = Scheduler.new(4) { |line| STDOUT.puts(line); STDOUT.flush }
while (line = STDIN.gets)
	@service.schedule(&setup(line))
end
@service.stop

