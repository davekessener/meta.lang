#ifndef META_LISP_H
#define META_LISP_H

#include "util.h"
#include "tml.h"
#include "decimal.h"
#include "string.h"
#include "s2f.h"

#define SYMBOL(s) ::lisp::Symbol<::tml::to_upper<STR( #s )>>
#define SYM SYMBOL

namespace lisp {

using namespace tml;

using decimal::to_f;
using decimal::to_i;

// primitives > macros > functions

// (eval list) -> (apply (eval (car list)) (cdr list))
// apply(fn : function, args : list) -> fn.call((map eval args))
// apply(fn : macro, args : list) -> (eval fn.call(args))
// apply(fn : primitive, args : list) -> fn.call(args)

template<typename>
struct Symbol;

template<char ... S>
struct Symbol<String<Char<S>...>>
{
	typedef String<Char<S>...> Literal;
};

template<typename>
struct DictBinding;

template<typename S>
struct DictBinding<Symbol<S>>
{
	template<typename T>
	struct Apply;

	template<typename SS, typename V>
	struct Apply<List<Symbol<SS>, V>>
		: Equals<
			to_lower<S>,
			to_lower<SS>
		>
	{
	};
};

typedef True _true;
typedef False _false;
//typedef SYMBOL(#T) _true;
//typedef SYMBOL(#F) _false;
typedef SYMBOL(NIL) _nil;
typedef SYMBOL(EVAL) _eval;
typedef SYMBOL(BEGIN) _begin;
typedef SYMBOL(DEFINE) _define;
typedef SYMBOL(RESERVE) _reserve;
typedef SYMBOL(QUOTE) _quote;
typedef SYMBOL(LAMBDA) _lambda;
typedef SYMBOL(MACRO) _macro;
typedef SYMBOL(IF) _if;
typedef SYMBOL(load) _load;
typedef SYMBOL(expand-macro) _expand_macro;

typedef SYMBOL(+) _add;
typedef SYMBOL(-) _sub;
typedef SYMBOL(*) _mul;
typedef SYMBOL(/) _div;
typedef SYMBOL(==) _eq;
typedef SYMBOL(<) _lt;
typedef SYMBOL(ORD) _ord;
typedef SYMBOL(CHAR) _char;
typedef SYMBOL(CONS) _cons;
typedef SYMBOL(CAR) _car;
typedef SYMBOL(CDR) _cdr;
typedef SYMBOL(STRING->SYMBOL) _s_to_sym;
typedef SYMBOL(->STRING) _to_s;
typedef SYMBOL(STRING->LIST) _s_to_list;
typedef SYMBOL(STRING->NUMBER) _s_to_num;
typedef SYMBOL(STRING-APPEND) _s_append;
typedef SYMBOL(type-of) _type_of;
typedef SYMBOL(NIL?) _nilp;
typedef SYMBOL(ZERO?) _zerop;
typedef SYMBOL(NEGATIVE?) _negativep;
typedef SYMBOL(LIST?) _listp;
typedef SYMBOL(BOOL?) _boolp;
typedef SYMBOL(STRING?) _stringp;
typedef SYMBOL(symbol?) _symbolp;
typedef SYMBOL(number?) _numberp;

typedef SYMBOL(LIST) _list;
typedef SYMBOL(COND) _cond;
typedef SYMBOL(LET) _let;
typedef SYMBOL(EXPORT) _export;
typedef SYMBOL(IMPORT) _import;
typedef SYMBOL(find) _find;
typedef SYMBOL(map) _map;
typedef SYMBOL(append) _append;
typedef SYMBOL(not) _not;
typedef SYMBOL(or) _or;
typedef SYMBOL(and) _and;
typedef SYMBOL(>) _gt;
typedef SYMBOL(>=) _gte;
typedef SYMBOL(<=) _lte;
typedef SYMBOL(digit?) _digitp;

template<typename ... T>
using _ = make_list<T...>;

template<typename ... T>
using __ = _<_quote, _<T...>>;

typedef SYMBOL(A) _a;
typedef SYMBOL(B) _b;
typedef SYMBOL(C) _c;
typedef SYMBOL(D) _d;
typedef SYMBOL(E) _e;
typedef SYMBOL(F) _f;
typedef SYMBOL(G) _g;
typedef SYMBOL(H) _h;
typedef SYMBOL(I) _i;
typedef SYMBOL(J) _j;
typedef SYMBOL(K) _k;
typedef SYMBOL(L) _l;
typedef SYMBOL(M) _m;
typedef SYMBOL(N) _n;
typedef SYMBOL(O) _o;
typedef SYMBOL(P) _p;
typedef SYMBOL(Q) _q;
typedef SYMBOL(R) _r;
typedef SYMBOL(S) _s;
typedef SYMBOL(T) _t;
typedef SYMBOL(U) _u;
typedef SYMBOL(V) _v;
typedef SYMBOL(W) _w;
typedef SYMBOL(X) _x;
typedef SYMBOL(Y) _y;
typedef SYMBOL(Z) _z;
typedef SYMBOL(0) _0;
typedef SYMBOL(1) _1;
typedef SYMBOL(2) _2;
typedef SYMBOL(3) _3;
typedef SYMBOL(4) _4;
typedef SYMBOL(5) _5;
typedef SYMBOL(6) _6;
typedef SYMBOL(7) _7;
typedef SYMBOL(8) _8;
typedef SYMBOL(9) _9;

// # ----------------------------------------------------------------------------------

template<typename, typename, typename>
struct Apply;

template<typename, typename, typename>
struct Eval;

template<template <typename, typename> class F>
struct Primitive
{
};

template<template <typename, typename> class F>
struct Buildin
{
};

template<typename A, typename B, typename E>
struct Closure
{
};

template<typename T, typename N>
struct LateBinding
{
};

template<typename A, typename B, typename E>
struct Macro
{
};

struct StringType  { typedef StringType  Type; };
struct NumberType  { typedef NumberType  Type; };
struct BoolType    { typedef BoolType    Type; };
struct SymbolType  { typedef SymbolType  Type; };
struct ListType    { typedef ListType    Type; };
struct ClosureType { typedef ClosureType Type; };
struct MacroType   { typedef MacroType   Type; };

template<typename V>
struct TypeOf
{
	template<typename T, bool = T::Value>
	static constexpr BoolType impl(T *, decltype(nullptr));

	static constexpr Nil impl(...);

	typedef decltype(impl((V *) nullptr, nullptr)) Type;

	STATIC_ASSERT((!Equals<Type, Nil>::Value), "TypeOf of invalid type!");
};

template<char ... S>
struct TypeOf<String<Char<S>...>> : StringType
{
};

template<bool S, typename E, typename M>
struct TypeOf<decimal::Float<S, E, M>> : NumberType
{
};

template<char ... S>
struct TypeOf<Symbol<String<Char<S>...>>> : SymbolType
{
};

template<typename H, typename T>
struct TypeOf<List<H, T>> : ListType
{
};

template<>
struct TypeOf<Nil> : ListType
{
};

template<typename P, typename B, typename X>
struct TypeOf<Closure<P, B, X>> : ClosureType
{
};

template<typename P, typename B, typename X>
struct TypeOf<Macro<P, B, X>> : MacroType
{
};

template<typename T, typename N>
struct TypeOf<LateBinding<T, N>> : TypeOf<T>
{
};

template<typename T>
using type_of = typename TypeOf<T>::Type;

// # ----------------------------------------------------------------------------------

template<typename T, typename E = Nil>
using eval = typename Eval<type_of<T>, T, E>::Type;

template<typename E>
struct Evaller
{
	template<typename T>
	struct Apply
	{
		typedef eval<T, E> Type;
	};
};

template<typename, typename, typename>
struct Overwrite;

template<typename K1, typename V1, typename T, typename K2, typename V2>
struct Overwrite<List<List<K1, V1>, T>, K2, V2>
{
	typedef List<
		List<K1, V1>,
		typename Overwrite<T, K2, V2>::Type
	> Type;
};

template<typename K, typename V1, typename T, typename V2>
struct Overwrite<List<List<K, V1>, T>, K, V2>
{
	typedef List<List<K, V2>, T> Type;
};

template<typename K, typename V>
struct Overwrite<Nil, K, V>
{
	typedef List<List<K, V>, Nil> Type;
};

template<typename, typename, typename>
struct Extend;

template<typename E, typename H, typename T, typename A>
struct Extend<E, List<Symbol<H>, T>, A>
{
	typedef typename Overwrite<E, Symbol<H>, car<A>>::Type env_t;
	typedef typename Extend<env_t, T, cdr<A>>::Type Type;
};

template<typename E, typename P, typename A>
struct Extend<E, Symbol<P>, A>
	: Overwrite<E, Symbol<P>, A>
{
};

template<typename E>
struct Extend<E, Nil, Nil>
{
	typedef E Type;
};

template<typename E, typename P, typename A>
using extend = typename Extend<E, P, A>::Type;

template<typename B, typename E>
struct Merge
{
	typedef typename Overwrite<B, car<car<E>>, cdr<car<E>>>::Type part_t;
	typedef typename Merge<part_t, cdr<E>>::Type Type;
};

template<typename B>
struct Merge<B, Nil>
{
	typedef B Type;
};

template<typename Base, typename Ext>
using merge = typename Merge<Base, Ext>::Type;

template<typename E, typename N>
using remove = reject<DictBinding<N>, E>;

// # ----------------------------------------------------------------------------------

template<template <typename, typename> class F, typename A, typename E>
struct Apply<Primitive<F>, A, E> : F<A, E>
{
};

template<template <typename, typename> class F, typename A, typename E>
struct Apply<Buildin<F>, A, E> : F<map<Evaller<E>, A>, E>
{
};

template<template <typename, typename, typename> class C, typename P, typename B, typename X, typename N, typename A, typename E>
struct BindingHelper
{
	typedef C<P, B, X> fun_t;
	typedef LateBinding<fun_t, N> bind_t;
	typedef extend<X, N, bind_t> env_t;

	typedef typename Apply<C<P, B, env_t>, A, E>::Type Type;
	typedef E Environment;
};

template<typename P, typename B, typename X, typename N, typename A, typename E>
struct Apply<LateBinding<Macro<P, B, X>, N>, A, E>
	: BindingHelper<Macro, P, B, X, N, A, E>
{
};

template<typename P, typename B, typename X, typename N, typename A, typename E>
struct Apply<LateBinding<Closure<P, B, X>, N>, A, E>
	: BindingHelper<Closure, P, B, X, N, A, E>
{
};

template<typename P, typename B, typename X, typename A, typename E>
struct Apply<Macro<P, B, X>, A, E>
{
	typedef merge<E, extend<X, P, A>> env_t;
	typedef eval<B, env_t> part_t;
	typedef Eval<type_of<part_t>, part_t, env_t> super_t;

	typedef typename super_t::Type Type;
	typedef typename super_t::Environment Environment;
};

template<typename P, typename B, typename X, typename A, typename E>
struct Apply<Closure<P, B, X>, A, E>
{
	typedef map<Evaller<E>, A> args_t;
	typedef extend<X, P, args_t> env_t;

	typedef eval<B, merge<E, env_t>> Type;
	typedef E Environment;
};

// # --------------------------------------------------------------------------------------------

namespace fn
{
	template<typename A, typename E>
	struct Evaluate
	{
		STATIC_ASSERT((Length<A>::Value == 1));

		typedef Eval<type_of<car<A>>, car<A>, E> tmp_t;
		typedef eval<
			typename tmp_t::Type,
			typename tmp_t::Environment
		> Type;
		typedef E Environment;
	};

	template<typename A, typename E, typename R>
	struct BeginImpl
	{
		typedef Eval<
			type_of<car<A>>,
			car<A>,
			E
		> Result;
		typedef BeginImpl<
			cdr<A>,
			typename Result::Environment,
			typename Result::Type
		> part_t;

		typedef typename part_t::Type Type;
		typedef typename part_t::Environment Environment;
	};

	template<typename E, typename R>
	struct BeginImpl<Nil, E, R>
	{
		typedef R Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Begin : BeginImpl<A, E, Nil>
	{
	};

	template<typename A, typename E>
	struct Define
	{
		static_assert(Length<A>::Value == 2, "(DEFINE sym val)");
		static_assert(Equals<type_of<car<A>>, SymbolType>::Value, "DEFINE requires symbol as name!");

		typedef car<A> name_t;
		typedef car<cdr<A>> body_t;
		typedef Eval<type_of<body_t>, body_t, E> val_t;

		template<typename T>
		struct impl
		{
			typedef T Type;
		};

		template<typename P, typename B, typename X>
		struct impl<Closure<P, B, X>>
		{
			typedef LateBinding<Closure<P, B, X>, name_t> Type;
		};

		template<typename P, typename B, typename X>
		struct impl<Macro<P, B, X>>
		{
			typedef LateBinding<Macro<P, B, X>, name_t> Type;
		};

		typedef typename impl<typename val_t::Type>::Type Type;
		typedef extend<typename val_t::Environment, name_t, Type> Environment;
	};

	template<typename A, typename E>
	struct Reserve
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, SymbolType>::Value));

		typedef car<A> Type;
		typedef remove<E, car<A>> Environment;
	};

	template<typename A, typename E>
	struct Quote
	{
		static_assert(Length<A>::Value == 1, "(QUOTE val)");

		typedef car<A> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Lambda
	{
		static_assert(Length<A>::Value == 2, "(LAMBDA (args) (body))");
		STATIC_ASSERT((Or<Equals<ListType, type_of<car<A>>>, Equals<SymbolType, type_of<car<A>>>>::Value));

		typedef Closure<car<A>, car<cdr<A>>, E> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct DefMacro
	{
		STATIC_ASSERT((Length<A>::Value == 2));
		STATIC_ASSERT((Equals<SymbolType, type_of<car<A>>>::Value));

		typedef Macro<car<A>, car<cdr<A>>, E> Type;
		typedef E Environment;
	};

	template<bool V, typename A, typename B, typename E>
	struct IfImpl
	{
		typedef eval<car<B>, E> Type;
	};

	template<bool V, typename A, typename E>
	struct IfImpl<V, A, Nil, E>
	{
		typedef Nil Type;
	};

	template<typename A, typename B, typename E>
	struct IfImpl<true, A, B, E>
	{
		typedef eval<A, E> Type;
	};

	template<typename T, typename = decltype(T::Value)>
	constexpr bool if_impl_helper(T *)
	{
		return T::Value;
	}

	constexpr bool if_impl_helper(...)
	{
		return true;
	}

	template<typename A, typename E>
	struct If
	{
		STATIC_ASSERT((Length<A>::Value >= 2));

		typedef typename IfImpl<
			if_impl_helper((eval<car<A>, E> *) 0),
			car<cdr<A>>,
			cdr<cdr<A>>,
			E
		>::Type Type;
		typedef E Environment;
	};

	template<typename T, typename N, typename E>
	struct ImportImpl
	{
		typedef cdr<T> Type;
	};

	template<typename N, typename E>
	struct ImportImpl<Nil, N, E>
	{
		typedef eval<_<_load, N>, E> Type;
	};

	template<typename A, typename E>
	struct Import
	{
		STATIC_ASSERT((Length<A>::Value == 2));
		STATIC_ASSERT((Equals<SymbolType, type_of<car<A>>>::Value));
		STATIC_ASSERT((Equals<SymbolType, type_of<car<cdr<A>>>>::Value));

		typedef car<A> lib_name_t;
		typedef car<cdr<A>> fun_name_t;
		typedef typename ImportImpl<
			opt_find<DictBinding<lib_name_t>, E>,
			lib_name_t,
			E
		>::Type lib_t;

		typedef cdr<find<DictBinding<fun_name_t>, lib_t>> Type;
		typedef extend<E, _<lib_name_t, fun_name_t>, _<lib_t, Type>> Environment;
	};

	template<typename, typename, typename>
	struct ExpandMacroImpl;

	template<typename P, typename B, typename X, typename A, typename E>
	struct ExpandMacroImpl<Macro<P, B, X>, A, E>
	{
		typedef merge<E, extend<X, P, A>> env_t;
		typedef eval<B, env_t> Type;
	};

	template<typename P, typename B, typename X, typename N, typename A, typename E>
	struct ExpandMacroImpl<LateBinding<Macro<P, B, X>, N>, A, E>
		: ExpandMacroImpl<Macro<P, B, X>, A, E>
	{
	};

	template<typename A, typename E>
	struct ExpandMacro
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<ListType, type_of<car<A>>>::Value));

		typedef eval<car<car<A>>, E> macro_t;
		typedef cdr<car<A>> args_t;
		typedef typename ExpandMacroImpl<macro_t, args_t, E>::Type Type;
	};

// # --------------------------------------------------------------------------------------------

	template<typename A, typename E>
	struct GetType
	{
		STATIC_ASSERT((Length<A>::Value == 1));

		template<typename, typename>
		struct impl;

		template<typename T>
		struct impl<ListType, T>
		{
			typedef STR("LIST") Type;
		};

		template<typename T>
		struct impl<NumberType, T>
		{
			typedef STR("NUMBER") Type;
		};

		template<typename T>
		struct impl<BoolType, T>
		{
			typedef STR("BOOL") Type;
		};

		template<typename T>
		struct impl<MacroType, T>
		{
			typedef STR("MACRO") Type;
		};

		template<typename T>
		struct impl<StringType, T>
		{
			typedef STR("STRING") Type;
		};

		template<typename T>
		struct impl<ClosureType, T>
		{
			typedef STR("CLOSURE") Type;
		};
		
		template<typename T>
		struct impl<SymbolType, T>
		{
			typedef STR("SYMBOL") Type;
		};

		typedef car<A> val_t;

		typedef typename impl<type_of<val_t>, val_t>::Type Type;
		typedef E Environment;
	};

	template
	<
		template <typename, typename> class Primary,
		template <typename, typename> class Secondary
	>
	struct Numeric
	{
		typedef Numeric<Primary, Secondary> Self;

		template<typename U, typename V>
		struct Apply
		{
			typedef Primary<U, V> Type;
		};

		template<typename A, typename E>
		struct Impl
		{
			static_assert(Length<A>::Value >= 2, "(+ a b ... c)");
			static_assert(All<predicates::Wrapper<decimal::IsFloat>, A>::Value, "Can only use numbers!");

			typedef reduce<Self, cdr<A>> tmp_t;
			typedef Secondary<car<A>, tmp_t> Type;
			typedef E Environment;
		};
	};

	template<typename A, typename E>
	using Add = Numeric<decimal::Add, decimal::Add>::template Impl<A, E>;

	template<typename A, typename E>
	using Sub = Numeric<decimal::Add, decimal::Sub>::template Impl<A, E>;

	template<typename A, typename E>
	using Mul = Numeric<decimal::Mul, decimal::Mul>::template Impl<A, E>;

	template<typename A, typename E>
	using Div = Numeric<decimal::Mul, decimal::Div>::template Impl<A, E>;

	template<template <typename> class F>
	struct Logic
	{
		template<typename A, typename E>
		struct Impl
		{
			STATIC_ASSERT((Length<A>::Value == 1));
			STATIC_ASSERT((decimal::IsFloat<car<A>>::Value));

			typedef F<car<A>> Type;
			typedef E Environment;
		};
	};

	template<typename A, typename E>
	using IsZero = Logic<decimal::IsZero>::template Impl<A, E>;

	template<typename A, typename E>
	using IsNegative = Logic<decimal::IsNegative>::template Impl<A, E>;

	template<typename A, typename E>
	struct Cons
	{
		STATIC_ASSERT((Length<A>::Value == 2));
		
		typedef List<car<A>, car<cdr<A>>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Car
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, ListType>::Value));

		typedef car<car<A>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Cdr
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, ListType>::Value));

		typedef cdr<car<A>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct String2Symbol
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, StringType>::Value));

		typedef Symbol<to_upper<car<A>>> Type;
		typedef E Environment;
	};

	template<typename, typename = String<>>
	struct StringAppendImpl;

	template<char ... S1, char ... S2, typename R>
	struct StringAppendImpl<List<String<Char<S2>...>, R>, String<Char<S1>...>>
		: StringAppendImpl<R, String<Char<S1>..., Char<S2>...>>
	{
	};

	template<typename S>
	struct StringAppendImpl<Nil, S>
	{
		typedef S Type;
	};

	template<typename A, typename E>
	struct StringAppend
	{
		STATIC_ASSERT((Length<A>::Value >= 1));
		STATIC_ASSERT((All<predicates::Wrapper<IsString>, A>::Value));

		typedef typename StringAppendImpl<A>::Type Type;
		typedef E Environment;
	};

	template<typename, typename>
	struct ToStringImpl;

	template<typename>
	struct ToTypelessStringImpl;

	template<bool>
	struct BoolToString
	{
		typedef STR("#F") Type;
	};

	template<>
	struct BoolToString<true>
	{
		typedef STR("#T") Type;
	};

	template<typename A, typename E>
	struct ToString
	{
		STATIC_ASSERT((Length<A>::Value == 1));

		template<typename T, typename TT = type_of<T>>
		static typename ToStringImpl<TT, T>::Type impl(T *, decltype(nullptr));

		template<typename T, bool V = T::Value>
		static typename BoolToString<V>::Type impl(T *, decltype(nullptr));

		template<typename T>
		static typename ToTypelessStringImpl<T>::Type impl(T *, ...);

		typedef decltype(impl((car<A> *) nullptr, nullptr)) Type;
		typedef E Environment;
	};

	template<typename T, bool>
	struct List2String : ToString<_<T>, Nil>
	{
	};

	template<typename T>
	struct List2String<T, true>
		: StringAppendImpl<make_list<
			String<Char<' '>, Char<'.'>, Char<' '>>,
			typename ToString<_<T>, Nil>::Type
		>>
	{
	};

	template<typename H, typename T, bool V>
	struct List2String<List<H, T>, V>
		: StringAppendImpl<make_list<
			String<Char<'('>>,
			typename List2String<H, false>::Type,
			typename List2String<T, true>::Type,
			String<Char<')'>>
		>>
	{
	};

	template<typename H, typename T>
	struct List2String<List<H, T>, true>
		: StringAppendImpl<make_list<
			String<Char<' '>>,
			typename List2String<H, false>::Type,
			typename List2String<T, true>::Type
		>>
	{
	};

	template<bool V>
	struct List2String<Nil, V>
	{
		typedef STR("NIL") Type;
	};

	template<>
	struct List2String<Nil, true>
	{
		typedef String<> Type;
	};

	template<typename S>
	struct ToStringImpl<StringType, S>
		: StringAppendImpl<make_list<
			String<Char<'"'>>,
			S,
			String<Char<'"'>>
		>>
	{
	};

	template<typename S>
	struct ToStringImpl<SymbolType, S>
	{
		typedef typename S::Literal Type;
	};

	template<typename F>
	struct ToStringImpl<NumberType, F> : Float2String<F>
	{
	};

	template<typename L>
	struct ToStringImpl<ListType, L> : List2String<L, false>
	{
	};


	template<typename T, typename N>
	struct ToTypelessStringImpl<LateBinding<T, Symbol<N>>>
		: StringAppendImpl<make_list<
			typename ToString<_<T>, Nil>::Type,
			String<Char<'#'>>,
			N
		>>
	{
	};

	template<typename P, typename B, typename X>
	struct ToTypelessStringImpl<Closure<P, B, X>>
	{
		typedef STR("Closure") Type;
	};

	template<typename>
	struct String2ListImpl;

	template<char ... S>
	struct String2ListImpl<String<Char<S>...>>
	{
		typedef make_list<String<Char<S>>...> Type;
	};

	template<typename A, typename E>
	struct String2List
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, StringType>::Value));

		typedef typename String2ListImpl<car<A>>::Type Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct String2Num
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<type_of<car<A>>, StringType>::Value));

		typedef s_to_f<car<A>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct IsNil
	{
		STATIC_ASSERT((Length<A>::Value == 1));

		typedef Equals<Nil, car<A>> Type;
		typedef E Environment;
	};

	template<typename I>
	struct OrdImpl : to_i<car<I>>
	{
	};

	template<>
	struct OrdImpl<Nil> : Int<0>
	{
	};

	template<typename A, typename E>
	struct Ord
	{
		STATIC_ASSERT((Length<A>::Value >= 1));
		STATIC_ASSERT((Equals<StringType, type_of<car<A>>>::Value));

		static constexpr char Value = string_ref<car<A>, OrdImpl<cdr<A>>>::Value;
		typedef to_f<Value,0> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct ToChar
	{
		STATIC_ASSERT((Length<A>::Value == 1));
		STATIC_ASSERT((Equals<NumberType, type_of<car<A>>>::Value));

		static constexpr char Value = (char) to_i<car<A>>::Value;
		typedef String<Char<Value>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Eq
	{
		STATIC_ASSERT((Length<A>::Value == 2));

		typedef Equals<car<A>, car<cdr<A>>> Type;
		typedef E Environment;
	};

	template<typename A, typename E>
	struct Lt
	{
		STATIC_ASSERT((Length<A>::Value == 2));
		STATIC_ASSERT((Equals<NumberType, type_of<car<A>>>::Value));
		STATIC_ASSERT((Equals<NumberType, type_of<car<cdr<A>>>>::Value));

		typedef decimal::LessThan<car<A>, car<cdr<A>>> Type;
		typedef E Environment;
	};
}

// # --------------------------------------------------------------------------------------------

template<typename T, typename L>
struct AddAll
	: AddAll<
		typename T::template Put<car<car<L>>, cdr<car<L>>>::Type,
		cdr<L>
	>
{
};

template<typename T>
struct AddAll<T, Nil>
{
	typedef T Type;
};

// # --------------------------------------------------------------------------------------------

typedef make_list<
//	List<_true,      True>,
//	List<_false,     False>,
	List<_nil,       Nil>,
	List<_eval,      Primitive<fn::Evaluate>>,
	List<_begin,     Primitive<fn::Begin>>,
	List<_define,    Primitive<fn::Define>>,
	List<_reserve,   Primitive<fn::Reserve>>,
	List<_quote,     Primitive<fn::Quote>>,
	List<_lambda,    Primitive<fn::Lambda>>,
	List<_macro,     Primitive<fn::DefMacro>>,
	List<_if,        Primitive<fn::If>>,
	List<_import,    Primitive<fn::Import>>,
	List<_expand_macro, Primitive<fn::ExpandMacro>>,
	List<_type_of,   Buildin<fn::GetType>>,
	List<_add,       Buildin<fn::Add>>,
	List<_sub,       Buildin<fn::Sub>>,
	List<_mul,       Buildin<fn::Mul>>,
	List<_div,       Buildin<fn::Div>>,
	List<_cons,      Buildin<fn::Cons>>,
	List<_car,       Buildin<fn::Car>>,
	List<_cdr,       Buildin<fn::Cdr>>,
	List<_zerop,     Buildin<fn::IsZero>>,
	List<_negativep, Buildin<fn::IsNegative>>,
	List<_to_s,      Buildin<fn::ToString>>,
	List<_s_to_sym,  Buildin<fn::String2Symbol>>,
	List<_s_to_list, Buildin<fn::String2List>>,
	List<_s_to_num,  Buildin<fn::String2Num>>,
	List<_s_append,  Buildin<fn::StringAppend>>,
	List<_nilp,      Buildin<fn::IsNil>>,
	List<_ord,       Buildin<fn::Ord>>,
	List<_char,      Buildin<fn::ToChar>>,
	List<_eq,        Buildin<fn::Eq>>,
	List<_lt,        Buildin<fn::Lt>>
> system_lib_t;

//typedef AddAll<
//	HashMap<Hasher>,
//	system_lib_t
//>::Type system_map_t;

// # ----------------------------------------------------------------------------------

template<typename S, typename E>
struct Eval<StringType, S, E>
{
	typedef S Type;
	typedef E Environment;
};

template<typename F, typename E>
struct Eval<NumberType, F, E>
{
	typedef F Type;
	typedef E Environment;
};

template<typename B, typename E>
struct Eval<BoolType, B, E>
{
	typedef B Type;
	typedef E Environment;
};

template<typename S, typename E>
struct Eval<SymbolType, S, E> : Cdr<find_any<DictBinding<S>, make_list<E, system_lib_t>>>
{
	typedef E Environment;
};

template<typename L, typename E>
struct Eval<ListType, L, E> : Apply<eval<car<L>, E>, cdr<L>, E>
{
};

// # ----------------------------------------------------------------------------------

template<typename T, typename E>
struct MacroExpand
{
	typedef T Type;
};

template<typename E>
struct MacroExpander
{
	template<typename T>
	struct Apply
		: MacroExpand<T, E>
	{
	};
};

template<typename V, typename N, typename R, typename E>
struct MacroExpandImpl
{
	typedef List<N, map<MacroExpander<E>, R>> Type;
};

template<typename P, typename B, typename X, typename N, typename R, typename E>
struct MacroExpandImpl<List<N, Macro<P, B, X>>, N, R, E>
{
	typedef eval<B, extend<E, P, R>> Type;
};

template<typename P, typename B, typename X, typename N, typename R, typename E>
struct MacroExpandImpl<List<N, LateBinding<Macro<P, B, X>, N>>, N, R, E>
{
	typedef Macro<P, B, X> fun_t;
	typedef LateBinding<fun_t, N> bind_t;
	typedef extend<extend<E, P, R>, N, bind_t> env_t;
	typedef eval<B, env_t> Type;
};

template<typename S, typename R, typename E>
struct MacroExpand<List<Symbol<S>, R>, E>
	: MacroExpandImpl<opt_find<DictBinding<Symbol<S>>, E>, Symbol<S>, R, E>
{
};

template<typename H, typename T, typename E>
struct MacroExpand<List<H, T>, E>
	: Map<MacroExpander<E>, List<H, T>>
{
};

template<typename T, typename E>
using macro_expand = typename MacroExpand<T, E>::Type;

// # ----------------------------------------------------------------------------------

typedef typename Eval<
	ListType,
	_<_begin,
		_<_define, _list, _<_lambda, _l, _l>>,
		_<_define, _cond, _<_macro, _l,
			_<_begin,
				_<_define, SYMBOL(impl), _<_lambda, _<_r>,
					_<_if, _<_nilp, _r>,
						_nil,
						_<_list,
							_<_quote, _if>,
							_<_car, _<_car, _r>>,
							_<_car, _<_cdr, _<_car, _r>>>,
							_<SYMBOL(impl), _<_cdr, _r>>>>>>,
				_<SYMBOL(impl), _l>>>>,
		_<_define, _map, _<_lambda, _<_f, _l>,
			_<_if, _<_nilp, _l>,
				_nil,
				_<_cons,
					_<_f, _<_car, _l>>,
					_<_map, _f, _<_cdr, _l>>>>>>,
		_<_define, _find, _<_lambda, _<_f, _l>,
			_<_if, _<_nilp, _l>,
				_nil,
				_<_if, _<_f, _<_car, _l>>,
					_<_car, _l>,
					_<_find, _f, _<_cdr, _l>>>>>>,
		_<_define, _export, _<_macro, _l,
			_<_cons,
				_<_quote, _list>,
				_<_map,
					_<_lambda, _<_e>,
						_<_list,
							_<_quote, _cons>,
							_<_list,
								_<_quote, _quote>,
								_<_car, _e>>,
							_<_car, _<_cdr, _e>>>>,
					_l>>>>,
		_<_define, _append, _<_lambda, _<_b, _l>,
			_<_if, _<_nilp, _b>,
				_l,
				_<_cons,
					_<_car, _b>,
					_<_append, _<_cdr, _b>, _l>>>>>,
		_<_define, _not, _<_lambda, _<_v>, _<_if, _v, _false, _true>>>,
		_<_define, _or, _<_lambda, _<_a, _b>,
			_<_if, _a, _true, _b>>>,
		_<_define, _and, _<_lambda, _<_a, _b>,
			_<_if, _a, _b, _false>>>,
		_<_define, _gt, _<_lambda, _<_a, _b>,
			_<_lt, _b, _a>>>,
		_<_define, _lte, _<_lambda, _<_a, _b>,
			_<_not, _<_lt, _b, _a>>>>,
		_<_define, _gte, _<_lambda, _<_a, _b>,
			_<_not, _<_lt, _a, _b>>>>,
		_<_define, _digitp, _<_lambda, _<_c>,
			_<_and, _<_gte, _c, to_f<'0'>>, _<_lte, _c, to_f<'9'>>>>>,
		_<_define, _let, _<_macro, _l,
			_<_cons,
				_<_quote, _begin>,
				_<_append,
					_<_map,
						_<_lambda, _<_p>,
							_<_list,
								_<_quote, _define>,
								_<_car, _p>,
								_<_car, _<_cdr, _p>>>>,
						_<_car, _l>>,
					_<_cdr, _l>>>>>>,
	Nil
>::Environment user_lib_t;


// # ----------------------------------------------------------------------------------

template<typename ... T>
using repl = eval<macro_expand<_<_begin, T...>, user_lib_t>, user_lib_t>;

template<typename ...>
struct ReplImpl;

template<typename B, typename K, typename V, typename E, typename ... R>
struct ReplImpl<B, List<List<K, V>, E>, R...>
	: ReplImpl<extend<B, K, V>, E, R...>
{
};

template<typename B, typename ... R>
struct ReplImpl<B, Nil, R...>
	: ReplImpl<B, R...>
{
};

template<typename B>
struct ReplImpl<B>
{
	typedef B Type;
};

template<typename ... E>
struct Repl
{
	typedef typename ReplImpl<user_lib_t, E...>::Type environment;

	template<typename ... T>
	struct Run
	{
		typedef eval<macro_expand<_<_begin, T...>, environment>, environment> Type;
	};

	template<typename T>
	struct Eval
	{
		typedef eval<T, environment> Type;
	};

	template<typename T>
	struct Expand
	{
		typedef macro_expand<T, environment> Type;
	};
};

}

#endif

