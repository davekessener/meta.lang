(begin
	(define sqrt (lambda (x)
		(begin
			(define impl (lambda (y)
				(begin
					(define new-y (/ (+ (/ x y) y) 2))
					(if (== y new-y)
						y
						(impl new-y)))))
			(impl (/ x 2)))))
	(sqrt 0.5))

