(begin
	(define cadr (lambda (l) (car (cdr l))))
	(define cddr (lambda (l) (cdr (cdr l))))
	(define caddr (lambda (l) (car (cddr l))))
	(define cdddr (lambda (l) (cdr (cddr l))))
	(define cadddr (lambda (l) (car (cdddr l))))

	(define bool?      (lambda (v) (== "BOOL"    (type-of v))))
	(define procedure? (lambda (v) (== "CLOSURE" (type-of v))))
	(define list?      (lambda (v) (== "LIST"    (type-of v))))
	(define symbol?    (lambda (v) (== "SYMBOL"  (type-of v))))
	(define empty? nil?)

	(define starts-with? (lambda (c s)
		(== c (ord (->string s) 0))))

	(define make-grammar (macro l
		(cons
			'list
			(map
				(lambda (e)
					(list
						'cons
						(list 'quote (car e))
						(if (starts-with? 63 (car e))
							(list
								'quote
								(car (cdr e)))
							(car (cdr e)))))
				l))))

	(define retrieve (lambda (l id)
		(cdr (find (lambda (e) (== (car e) id)) l))))

	(define first-valid (lambda (l f)
		(if (empty? l)
			#f
			(let (
				(r
					(f (car l)))
				(c
					(cdr l))
			)
				(if r
					r
					(first-valid c f))))))

	(define impl (lambda (result stack e grammar)
		(cond
			((and (empty? stack) (empty? e))
				result)
			((empty? stack)
				#f)
			(#t
				(let (
					(token
						(car stack))
					(new-stack
						(cdr stack))
				)
					(cond
						((symbol? token)
							(let (
								(body
									(retrieve grammar token))
								(is-command?
									(starts-with? 33 token))
								(is-query?
									(starts-with? 63 token))
							)
								(impl
									(if is-command?
										(body result)
										result)
									(if is-command?
										new-stack
										((if is-query?
											append
											cons)
												body
												new-stack))
									e
									grammar)))
						((list? token)
							(first-valid
								token
								(lambda (p)
									(impl
										result
										(append p new-stack)
										e
										grammar))))
						((procedure? token)
							(and
								(not (empty? e))
								(let (
									(r
										(token (car e)))
								)
									(if r
										(impl
											(cons
												(if (bool? r)
													(car e)
													r)
												result)
											new-stack
											(cdr e)
											grammar)
										#f))))
						(#t
							#f)))))))
	(define parse (lambda (g e)
		(impl nil '(?) e g)))

	(define math-g (make-grammar
		(? (?e))
		(?e (?c "+" ?e !))
		(?c (num))
		(num (lambda (e) (number? e)))
		(! (lambda (r) (cons (list (cadr r) (car r) (caddr r)) (cdddr r))))))

	(parse math-g '(1))
)
