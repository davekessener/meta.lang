(begin
	(define cadr (lambda (l) (car (cdr l))))
	(define cddr (lambda (l) (cdr (cdr l))))
	(define caddr (lambda (l) (car (cddr l))))
	(define cdddr (lambda (l) (cdr (cddr l))))
	(define cadddr (lambda (l) (car (cdddr l))))

	(define bool?      (lambda (v) (== "BOOL"    (type-of v))))
	(define procedure? (lambda (v) (== "CLOSURE" (type-of v))))
	(define list?      (lambda (v) (== "LIST"    (type-of v))))
	(define symbol?    (lambda (v) (== "SYMBOL"  (type-of v))))
	(define empty? nil?)

	(define starts-with? (lambda (c s)
		(== c (ord (->string s) 0))))

	(define make-grammar (macro l
		(cons
			'list
			(map
				(lambda (e)
					(list
						'cons
						(list 'quote (car e))
						(if (starts-with? 63 (car e))
							(list
								'quote
								(car (cdr e)))
							(car (cdr e)))))
				l))))

	(define parse (begin
		(define retrieve (lambda (l id)
			(cdr (find (lambda (e) (== (car e) id)) l))))

		(define first-valid (lambda (l f)
			(if (empty? l)
				#f
				(let (
					(r
						(f (car l)))
					(c
						(cdr l))
				)
					(if r
						r
						(first-valid c f))))))
		(define impl-symbol (lambda (e grammar token result stack)
			(let (
				(body
					(retrieve grammar token))
				(is-command?
					(starts-with? 33 token))
				(is-query?
					(starts-with? 63 token))
			)
				(list
					(if is-command?
						(body result)
						result)
					(if is-command?
						stack
						((if is-query?
							append
							cons)
								body
								stack))
					e
					grammar))))
		(define impl (lambda (result stack e grammar)
			(cond
				((and (empty? stack) (empty? e))
					result)
				((empty? stack)
					#f)
				(#t
					(let (
						(token
							(car stack))
						(new-stack
							(cdr stack))
					)
						(cond
							((symbol? token)
								(apply impl (impl-symbol e grammar token result new-stack)))
							((list? token)
								nil)
							((procedure? token)
								nil)
							(#t
								(list 'error token))))))))
		impl-symbol
	))

	(define math-g (make-grammar
		(? (?a))
		(?a (c addsub ?a !))
		(c (lambda (e) (number? e)))
		(addsub (lambda (e) (or (== e '+) (== e '-))))
		(! (lambda (r) (cons (list (cadr r) (car r) (caddr r)) (cdddr r))))))

	(parse '() math-g '? '() '())
)
