(begin
	(define impl (lambda (n)
		(if (zero? n)
			1
			(*
				n
				(impl (- n 1))))))
	(export
		(fact impl)))
