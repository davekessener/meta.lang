#ifndef META_TML_H
#define META_TML_H

#include "util.h"

namespace tml {

// # ================================================================================
// # --- Primitives -----------------------------------------------------------------
// # ================================================================================

struct Nil { };

template<typename T>
struct Type2Type
{
	typedef T Type;
};

template<bool V>
struct Bool
{
	static constexpr bool Value = V;
	typedef Bool<V> Type;
	template<bool VV> struct Apply : Bool<VV> { };
};

typedef Bool<true> True;
typedef Bool<false> False;

template<int I>
struct Int
{
	static constexpr int Value = I;
	typedef Int<I> Type;
	template<int II> struct Apply : Int<II> { };
};

template<typename A, typename B>
struct Add : A::template Apply<A::Value + B::Value>
{
};

template<typename A>
struct Negate : A::template Apply<-A::Value>
{
};

// # ================================================================================
// # --- Logic ----------------------------------------------------------------------
// # ================================================================================

template<typename, typename>
struct Equals : False
{
};

template<typename T>
struct Equals<T, T> : True
{
};

template<typename V>
struct Not : Bool<(!V::Value)>
{
};

template<typename ...>
struct And;

template<typename V, typename ... R>
struct And<V, R...> : Bool<(V::Value && And<R...>::Value)>
{
};

template<>
struct And<> : True
{
};

template<typename ...>
struct Or;

template<typename V, typename ... R>
struct Or<V, R...> : Bool<(V::Value || Or<R...>::Value)>
{
};

template<>
struct Or<> : False
{
};

template<bool, typename T1, typename>
struct IfImpl
{
	typedef typename T1::Type Type;
};

template<typename T1, typename T2>
struct IfImpl<false, T1, T2>
{
	typedef typename T2::Type Type;
};

template
<
	typename Condition,
	typename IfClause,
	typename ElseClause
>
struct If : IfImpl<Condition::Value, IfClause, ElseClause>
{
};

// # ================================================================================
// # --- List -----------------------------------------------------------------------
// # ================================================================================

template<typename H, typename T>
struct List
{
	typedef H Head;
	typedef T Tail;
};

// # --------------------------------------------------------------------------------

template<typename L>
struct Car
{
	typedef typename L::Head Type;
};

template<typename L>
struct Cdr
{
	typedef typename L::Tail Type;
};

template<typename T>
using car = typename T::Head;

template<typename T>
using cdr = typename T::Tail;

// # --------------------------------------------------------------------------------

template<typename ... T>
struct ListMaker;

template<typename T, typename ... TT>
struct ListMaker<T, TT...>
{
	typedef List<T, typename ListMaker<TT...>::Type> Type;
};

template<>
struct ListMaker<>
{
	typedef Nil Type;
};

template<typename ... T>
using make_list = typename ListMaker<T...>::Type;

// # --------------------------------------------------------------------------------

template<typename L>
struct Length : Int<Length<cdr<L>>::Value + 1>
{
};

template<>
struct Length<Nil> : Int<0>
{
};

// # --------------------------------------------------------------------------------

template<typename L, typename I>
struct ListNth : If<I, ListNth<cdr<L>, Int<I::Value - 1>>, Car<L>>
{
	static_assert(I::Value >= 0, "Cannot get element w negative index!");
	static_assert(I::Value < Length<L>::Value, "Cannot access elements beyond list length!");
};

template<typename L, typename I>
using nth = typename ListNth<L, I>::Type;

// # --------------------------------------------------------------------------------

template
<
	typename F,
	typename L
>
struct IndexOf : If<
	typename F::template Apply<car<L>>,
	Int<0>,
	Add<IndexOf<F, cdr<L>>, Int<1>>>::Type
{
	static_assert(!Equals<L, Nil>::Value, "No such element!");
};

template<typename F, typename L>
using index_of = typename IndexOf<F, L>::Type;

// # --------------------------------------------------------------------------------

template
<
	typename F,
	typename L
>
struct Find : ListNth<L, index_of<F, L>>
{
};

template<typename F, typename L>
using find = typename Find<F, L>::Type;

template
<
	typename F,
	typename L,
	typename D = Nil
>
struct OptFind
	: If<
		typename F::template Apply<car<L>>,
		Car<L>,
		OptFind<F, cdr<L>, D>
	>
{
};

template<typename F, typename D>
struct OptFind<F, Nil, D>
{
	typedef D Type;
};

template<typename F, typename L, typename D = Nil>
using opt_find = typename OptFind<F, L, D>::Type;

template
<
	typename F,
	typename L,
	typename D = Nil
>
struct FindAny
{
	STATIC_ASSERT((!Equals<L, Nil>::Value));

	typedef opt_find<F, car<L>, D> match_t;
	typedef typename If<
		Equals<D, match_t>,
		FindAny<F, cdr<L>, D>,
		Type2Type<match_t>
	>::Type Type;
};

template<typename F, typename L, typename D = Nil>
using find_any = typename FindAny<F, L, D>::Type;

// # --------------------------------------------------------------------------------

template<typename F, typename L, typename ... A>
struct ToVAImpl : ToVAImpl<F, cdr<L>, A..., car<L>>
{
};

template<typename F, typename ... A>
struct ToVAImpl<F, Nil, A...> : F::template Apply<A...>
{
};

template<typename F, typename L>
struct ToVA : ToVAImpl<F, L>
{
};

template<typename F, typename L>
using to_va = typename ToVA<F, L>::Type;

// # --------------------------------------------------------------------------------

template<typename L, int I1, int I2>
struct SubListImpl : SubListImpl<cdr<L>, I1 - 1, I2 - 1>
{
};

template<typename L, int I>
struct SubListImpl<L, 0, I>
{
	typedef List<car<L>, typename SubListImpl<cdr<L>, 0, I - 1>::Type> Type;
};

template<typename L>
struct SubListImpl<L, 0, 0>
{
	typedef Nil Type;
};

template<typename L, typename I1, typename I2>
struct SubList : SubListImpl<L, I1::Value, I2::Value>
{
	static_assert(I1::Value >= 0, "I1 cant be negative!");
	static_assert(I2::Value >= 0, "I2 cant be negative!");
	static_assert(I1::Value <= I2::Value, "I1 cant be larger than I2!");
	static_assert(I1::Value <= Length<L>::Value, "Index I1 out of range!");
	static_assert(I2::Value <= Length<L>::Value, "Index I2 out of range!");
};

template<typename L, typename I1, typename I2>
using sub_list = typename SubList<L, I1, I2>::Type;

template<typename L, typename I>
struct RemoveFirst : SubList<L, I, Length<L>>
{
};

template<typename L, typename I>
using remove_first = typename RemoveFirst<L, I>::Type;

template<typename L, typename I>
struct RemoveLast : SubList<L, Int<0>, Add<Length<L>, Negate<I>>>
{
};

template<typename L, typename I>
using remove_last = typename RemoveLast<L, I>::Type;

// # --------------------------------------------------------------------------------

template<bool V, typename F, typename L>
struct AllImpl : False
{
};

template<typename F, typename L>
struct AllImpl<true, F, L> : AllImpl<F::template Apply<car<L>>::Type::Value, F, cdr<L>>
{
};

template<typename F>
struct AllImpl<true, F, Nil> : True
{
};

template<typename F, typename L>
struct All : AllImpl<true, F, L>
{
};

template<bool V, typename F, typename L>
struct AnyImpl : True
{
};

template<typename F, typename L>
struct AnyImpl<false, F, L> : AnyImpl<F::template Apply<car<L>>::Type::Value, F, cdr<L>>
{
};

template<typename F>
struct AnyImpl<false, F, Nil> : False
{
};

template<typename F, typename L>
struct Any : AnyImpl<false, F, L>
{
};

// # --------------------------------------------------------------------------------

template<typename F, typename L>
struct Map
{
	typedef typename F::template Apply<L>::Type Type;
};

template<typename F, typename H, typename T>
struct Map<F, List<H, T>>
{
	typedef List<
		typename F::template Apply<H>::Type,
		typename Map<F, T>::Type
	> Type;
};

template<typename F>
struct Map<F, Nil>
{
	typedef Nil Type;
};

template<typename F, typename L>
using map = typename Map<F, L>::Type;

// # --------------------------------------------------------------------------------

template<typename F, typename L>
struct Reduce : F::template Apply<car<L>, typename Reduce<F, cdr<L>>::Type>
{
};

template<typename F, typename T>
struct Reduce<F, List<T, Nil>>
{
	typedef T Type;
};

template<typename F, typename L>
using reduce = typename Reduce<F, L>::Type;

// # --------------------------------------------------------------------------------

template<typename, typename>
struct Reject;

template<bool, typename F, typename T, typename L>
struct RejectImpl
{
	typedef List<T, typename Reject<F, L>::Type> Type;
};

template<typename F, typename T, typename L>
struct RejectImpl<true, F, T, L>
	: Reject<F, L>
{
};

template<typename F, typename H, typename L>
struct Reject<F, List<H, L>>
	: RejectImpl<F::template Apply<H>::Value, F, H, L>
{
};

template<typename F>
struct Reject<F, Nil>
{
	typedef Nil Type;
};

template<typename F, typename L>
using reject = typename Reject<F, L>::Type;

// # --------------------------------------------------------------------------------

template<typename L1, typename L2>
struct Append
{
	typedef List<car<L1>, typename Append<cdr<L1>, L2>::Type> Type;
};

template<typename L>
struct Append<Nil, L>
{
	typedef L Type;
};

template<typename L1, typename L2>
using append = typename Append<L1, L2>::Type;

// # --------------------------------------------------------------------------------

namespace predicates
{
	template<typename T>
	struct Equals
	{
		template<typename TT>
		struct Apply : ::tml::Equals<T, TT>
		{
		};
	};

	template<template <typename ...> class F>
	struct VAWrapper
	{
		template<typename ... T>
		struct Apply
		{
			typedef F<T...> Type;
		};
	};

	template<template <typename> class F>
	struct Wrapper
	{
		template<typename T>
		struct Apply
		{
			typedef F<T> Type;
		};
	};

	struct Identity
	{
		template<typename T>
		struct Apply
		{
			typedef T Type;
		};
	};
}

}

#endif

