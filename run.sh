#\!/bin/bash

time make run
r=$?

echo -e "\a"

if [ $r -ne 0 ]; then
	ruby translate.rb < log.cml | tee log_short.cml | ruby -e 'i = 0; while (line = STDIN.gets); line.strip!; puts "#{"%04i" % i} | #{line[0...200]} ..."; STDOUT.flush; i += 1; end' | tee log_crop.cml | less
fi

exit $r

