#!/usr/bin/env ruby

class Tokenizer
	def initialize(s)
		@source = s
		@idx = 0

		skip
	end

	def look
		raise if done?

		@source[@idx]
	end

	def next
		raise if done?

		@idx += 1
	end

	def done?
		@idx >= @source.length
	end

	def skip
		self.next while not done? and look == ' '
	end
end

INDENT = '    '

def parse(t, indent = 0)
	r = nil

	unless t.done?
		case t.look
			when '('
				r = []
				t.next
				t.skip
				while t.look != ')'
					r.push(parse(t, indent + 1).strip)
				end
				t.next

				r = "_<#{r.join(",\n#{INDENT * (indent + 1)}")}>"

			when "'"
				t.next
				r = "_<_quote, #{parse(t, indent + 1).strip}>"

			when '"'
				r = ''
				loop do
					t.next
					break if t.look == '"'
					r += t.look
				end
				t.next

				r = "STR(\"#{r}\")"

			else
				r = ''
				until t.done? or t.look == ' ' or t.look == ')'
					r += t.look
					t.next
				end

				if r =~ /^-?[0-9]+(\.[0-9]+)?$/
					d, f = *r.split('.')
					r = "to_f<#{d},#{f || '0'}>"
				elsif r.downcase == '#t'
					r = 'True'
				elsif r.downcase == '#f'
					r = 'False'
				else
					r = "SYM(#{r})" 
				end
		end
	end

	t.skip

	"#{INDENT * indent}#{r}"
end

if ARGV.length > 0
	name = ARGV[0]
	src = File.read("src/#{name}.cl").gsub(/[\n\t]/, ' ')
	abs = parse(Tokenizer.new(src))
	File.open("bin/#{name}.h", 'w') do |f|
		f.write(
%{
#ifndef META_BIN_#{name.upcase}_H
#define META_BIN_#{name.upcase}_H

#include "lisp.h"

namespace payload {

using namespace tml;
using namespace lisp;

typedef #{abs} #{name}_t;

}

#endif
}.strip)
	end
end

