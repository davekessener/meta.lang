#ifndef META_STRING_H
#define META_STRING_H

#include "util.h"
#include "tml.h"

namespace tml {

// # ================================================================================
// # --- Strings --------------------------------------------------------------------
// # ================================================================================

template<char C>
struct Char
{
	static constexpr char Value = C;
};

template<typename ...>
struct String;

template<char ... C>
struct String<Char<C>...>
{
	static constexpr char Value[] = { C..., '\0' };
};
template<char ... C> constexpr char String<Char<C>...>::Value[];

// # --------------------------------------------------------------------------------

template<char ... C>
struct StringFitter
{
	struct Body
	{
		template<typename ... T>
		struct Apply
		{
			typedef String<T...> Type;
		};
	};

	typedef predicates::Equals<Char<0>> pred_t;
	typedef make_list<Char<C>...> clist_t;
	typedef sub_list<clist_t, Int<0>, index_of<pred_t, clist_t>> str_t;
	typedef to_va<Body, str_t> Type;
};

template<char ... C>
using to_string = typename StringFitter<C...>::Type;

// # --------------------------------------------------------------------------------

template<typename S>
struct StringLength : Int<sizeof(S::Value)>
{
};

template<typename S>
using string_length = StringLength<S>;

template<typename S, typename I>
struct StringRef
{
	typedef Char<S::Value[I::Value]> Type;
};

template<typename S, typename I>
using string_ref = typename StringRef<S, I>::Type;

template<typename>
struct IsString : False
{
};

template<char ... S>
struct IsString<String<Char<S>...>> : True
{
};

// # --------------------------------------------------------------------------------

template<typename ... S>
struct Join;

template<char ... A, char ... B, typename ... R>
struct Join<String<Char<A>...>, String<Char<B>...>, R...>
	: Join<String<Char<A>..., Char<B>...>, R...>
{
};

template<char ... S>
struct Join<String<Char<S>...>>
{
	typedef String<Char<S>...> Type;
};

template<typename ... S>
using join = typename Join<S...>::Type;
// # --------------------------------------------------------------------------------

template<typename>
struct ToLower;

template<typename>
struct ToUpper;

constexpr char to_lower_helper(char c) { return (c >= 'A' && c <= 'Z') ? (c - 'A' + 'a') : c; }
constexpr char to_upper_helper(char c) { return (c >= 'a' && c <= 'z') ? (c - 'a' + 'A') : c; }

template<char ... C>
struct ToLower<String<Char<C>...>>
{
	typedef String<Char<to_lower_helper(C)>...> Type;
};

template<char ... C>
struct ToUpper<String<Char<C>...>>
{
	typedef String<Char<to_upper_helper(C)>...> Type;
};

template<typename S>
using to_lower = typename ToLower<S>::Type;

template<typename S>
using to_upper = typename ToUpper<S>::Type;

// # --------------------------------------------------------------------------------

constexpr char str_part(const char * const s, int i)
{
	return (i <= 0) ? (*s == '\t' ? ' ' : *s) : (*s ? str_part(s + 1, i - 1) : 0);
}

#define STR1(s,i) ::tml::str_part((s),(i)+0)
#define STR2(s,i) STR1((s),(i)+0),STR1((s),(i)+1)
#define STR4(s,i) STR2((s),(i)+0),STR2((s),(i)+2)
#define STR8(s,i) STR4((s),(i)+0),STR4((s),(i)+4)
#define STR16(s,i) STR8((s),(i)+0),STR8((s),(i)+8)
#define STR32(s,i) STR16((s),(i)+0),STR16((s),(i)+16)
#define STR64(s,i) STR32((s),(i)+0),STR32((s),(i)+32)
#define STR128(s,i) STR64((s),(i)+0),STR64((s),(i)+64)
#define STR(s) ::tml::to_string<STR128((s),0)>

}

#endif

