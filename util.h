#ifndef META_UTIL_H
#define META_UTIL_H

#include <stdint.h>

typedef unsigned uint;

#define MXT_TO_S_IMPL(x) #x
#define MXT_TO_S(x) MXT_TO_S_IMPL(x)
#define MXT_FILELINE(f,l) f ": " l
#define FILELINE __FILE__ ": " MXT_TO_S(__LINE__)
#define STATIC_ASSERT(v,...) static_assert(v,__VA_ARGS__ " [" FILELINE "]")

#endif

