#include <iostream>
#include <sstream>
#include <vector>
#include <iomanip>

#include "util.h"
#include "tml.h"
#include "string.h"
#include "decimal.h"
#include "lisp.h"

#include "bin/example.h"
#include "bin/numeric.h"
#include "bin/tmp.h"

// # ================================================================================
// # --- Main -----------------------------------------------------------------------
// # ================================================================================

int main(int argc, char **argv)
{
	using namespace tml;
	using namespace lisp;
	using namespace decimal;

	typedef repl<_<_to_s, payload::example_t>> result_t;

	std::cout << std::boolalpha << std::setprecision(10);

	std::cout << STR("Hello, World!")::Value << "\n"
	          << FILELINE << "\n"
	          << __TIME__ << "\n"
	          << std::endl;

	std::cout << result_t::Value << std::endl;

	return 0;
}

