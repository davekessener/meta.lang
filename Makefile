#CC=x86_64-w64-mingw32-g++
CC=g++
SRC=$(wildcard test/*.cc)
OBJ=$(SRC:.cc=.o)
DEP=$(wildcard *.h)
LISP_SRC=$(wildcard src/*.cl)
LISP_BIN=$(patsubst src/%.cl,bin/%.h,$(LISP_SRC))
CFLAGS=-Wall -O3 -ftemplate-depth=1000000 -I.
TARGET=Meta.exe

.PHONY: all run clean

test/%.o: test/%.cc %.h
	@rm -f *.cml
	$(CC) $< -c $(CFLAGS) -o $@ 2> log.cml

bin/%.h: src/%.cl
	@mkdir -p $(dir $@)
	./convert.rb $(basename $(notdir $@))

all: $(TARGET)

run: $(TARGET)
	./$(TARGET)

$(TARGET): $(OBJ) $(DEP) $(LISP_BIN) main.cc
	$(CC) main.cc $(CFLAGS) -o $(TARGET) $(LIBS) 2> log.cml
	@rm -f *.cml

clean:
	find . -name "*.o" | xargs rm -f
	rm -rf bin/
	rm -f $(TARGET)
	rm -f *.cml

